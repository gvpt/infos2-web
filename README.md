Infos2: The Intelligent Communication Platform
==============
An effective and fast information sharing is a hit of the modern age. The access to information from almost everywhere, anytime and without any dependence on the device is today a common thing.

My objective was to create an  information system for teachers and school management, which will collect actual announcements about interesting events, substitution tables and important documents for teachers.

I created a custom application core, which closely communicates with Google Cloud. The system is running on the school server and has been written in PHP. Events and substitution records are synchronized with Google Calendar.  Due to this fact the system is able to send text messages to the cell phones of the users.

I, successfully, created a modern information system which effectively shares information between teachers and school management very fast. At the same time, we have improved information awareness of the school employees.

LICENSE
--------------
This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.

REQUIREMENTS
------------
- PHP 5.X
- Phalcon framework
- PHP CURL
- MysqlDB
- Apache -> modrewrite

INSTALLATION
------------
1. Clone repo
2. In mysql create database and user for infos
3. Import database.sql from folder database in infos database
4. Move folder web where you want to have infos
5. Make sure that webserver can write to web/app/cache/volt and web/app/logs/db.log
6. Set values in web/app/config/config.php
7. Run composer install in /web
8. In /web/vendor/google/apiclient/src/Google/IO/Curl.php add to 'curl_setopt' this 'curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);'
9. Create .env file in /web
10. Copy content from .env.example to .env
11. Change values to fit you