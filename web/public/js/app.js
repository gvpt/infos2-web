$(document).ready(function() {
	$(document).foundation();

	$('table').delegate('tr[data-url]', 'click', function() {
		window.location.href = $(this).attr('data-url');
	});

	$('table').delegate('tr[data-ajax]', 'click', function() {
		$('#myModal').foundation('reveal', 'open', $(this).attr('data-ajax'));
	});

	$("#suploFilter_date").change(function(e){
		e.preventDefault();
		window.location.href = $("#formSuploFilter").attr('action') + moment($(this).val(), "DD.MM.YYYY").format("YYYY-MM-DD");
	});

	$("#suploHistory_season").change(function(){
		var form = $(this).parent();
		$("#printSummary").attr("href", form.attr("data-action-url") + $(this).val());
		form.attr("action", form.attr("data-action-url") + $(this).val());
		form.submit();
	});

	$("#newSuplo_date").change(function(){
		$.ajax({
			type: 'GET',
			url: $(this).attr('data-check-url') + $(this).val(),
			dataType: 'json',
			success: function(data) {
				console.log(data.text);
				if (data.exists) {
					$('#suploExists').html(data.text).parent('div').fadeIn('fast');
				}
				else {
					$('#suploExists').parent('div').fadeOut('fast');
				}
			}
		});
	});

	$("#formSuploHistory").submit(function(e){
		e.preventDefault();
		$.ajax({
			type: 'GET',
			url: $(this).attr('action'),
			dataType: 'html',
			success: function(data) {
				$("#suploHistory").html(data);
			},
			error: function () {
				alert("Error occured while loading substitution records");
			}
		});
	});

});


$(window).bind("load", function () {
	var footer = $("#footer");
	var pos = footer.position();
	var height = $(window).height() - 20;
	height = height - pos.top;
	height = height - footer.height();
	if (height > 0) {
		footer.css({
			'margin-top': height + 'px'
		});
	}
});

