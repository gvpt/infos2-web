/**
 * Created by kak3n on 18.9.2016.
 */
$('#newEvent_public').click(function() {
    if (this.checked) {
        $('#shortDescription').removeClass('hide-for-small-up');
    } else {
        el = document.getElementById('shortDescription');
        if (!el.classList.contains("hide-for-small-up")) {
            $('#shortDescription').addClass('hide-for-small-up');
        }
    }
});
$('#editEvent_public').click(function() {
    if (this.checked) {
        $('#shortDescription').removeClass('hide-for-small-up');
    } else {
        el = document.getElementById('shortDescription');
        if (!el.classList.contains("hide-for-small-up")) {
            $('#shortDescription').addClass('hide-for-small-up');
        }
    }
});