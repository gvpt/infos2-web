<?php

error_reporting(E_ALL);

try {

    /**
     * Composer autoload
     */
    include __DIR__ . '/../vendor/autoload.php';

    $dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
    $dotenv->load();

	define('BASE_DIR', dirname(__DIR__));
	define('APP_DIR', BASE_DIR . '/app');

	/**
	 * Read the configuration
	 */
	$config = include(APP_DIR . '/config/config.php');

	/**
	 * Read auto-loader
	 */
	include APP_DIR . "/config/loader.php";

	/**
	 * Read services
	 */
	include APP_DIR . "/config/services.php";

	/**
	 * Handle the request
	 */

	define('BASE_URL', $config->application->baseUri);
	$application = new \Phalcon\Mvc\Application($di);

	echo $application->handle()->getContent();

} catch (\Exception $e) {
	echo $e->getMessage() . '<br>';
	echo '<pre>' . $e->getTraceAsString() . '</pre>';
}