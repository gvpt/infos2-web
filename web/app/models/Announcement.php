<?php

class Announcement extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_announcement;

    /**
     *
     * @var string
     */
    public $id_user;

    /**
     *
     * @var string
     */
    public $ann_created;

    /**
     *
     * @var string
     */
    public $ann_updated;

    /**
     *
     * @var string
     */
    public $ann_title;

    /**
     *
     * @var string
     */
    public $ann_text;

    /**
     *
     * @var string
     */
    public $ann_deadline;

    /**
     *
     * @var string
     */
    public $ann_public;

    /**
     *
     * @var string
     */
    public $ann_uid;

    /**
     *
     * @var integer
     */
    public $ann_foreignerId;

    /**
     * @return Announcement[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return Announcement
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_announcement' => 'id_announcement', 
            'id_user' => 'id_user', 
            'ann_created' => 'ann_created', 
            'ann_updated' => 'ann_updated', 
            'ann_title' => 'ann_title', 
            'ann_text' => 'ann_text', 
            'ann_deadline' => 'ann_deadline',
            'ann_public' => 'ann_public',
            'ann_uid' => 'ann_uid',
            'ann_foreignerId' => 'ann_foreignerId'
        );
    }

	public function initialize() {
		$this->belongsTo('id_user', 'User', 'id_user', [
			'alias' => 'User'
		]);
	}

}
