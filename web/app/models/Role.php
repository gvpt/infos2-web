<?php

use Phalcon\Mvc\Model;

class Role extends Model {

    /**
     *
     * @var integer
     */
    public $id_role;

    /**
     *
     * @var string
     */
    public $rol_name;

    /**
     *
     * @var integer
     */
    public $rol_active;

	/**
	 * @param array $parameters
	 * @return Role[]
	 */
    public static function find($parameters = [])
    {
        return parent::find($parameters);
    }

	/**
	 * @param array $parameters
	 * @return Role
	 */
    public static function findFirst($parameters = [])
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id_role' => 'id_role', 
            'rol_name' => 'rol_name', 
            'rol_active' => 'rol_active'
        );
    }

	public function initialize() {
		$this->hasMany('id_role', 'Permission', 'id_role', [
			'alias' => 'Permission'
		]);
		$this->hasMany('id_role', 'User', 'id_role', [
			'alias' => 'User'
		]);
	}

}
