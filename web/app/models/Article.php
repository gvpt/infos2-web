<?php

use Phalcon\Filter;

class Article extends \Phalcon\Mvc\Model
{

    private $filter;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $asset_id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $alias;

    /**
     *
     * @var string
     */
    public $introtext;

    /**
     *
     * @var string
     */
    public $fulltext;

    /**
     *
     * @var integer
     */
    public $state;

    /**
     *
     * @var integer
     */
    public $catid;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var integer
     */
    public $created_by;

    /**
     *
     * @var string
     */
    public $created_by_alias;

    /**
     *
     * @var string
     */
    public $modified;

    /**
     *
     * @var integer
     */
    public $modified_by;

    /**
     *
     * @var integer
     */
    public $checked_out;

    /**
     *
     * @var string
     */
    public $checked_out_time;

    /**
     *
     * @var string
     */
    public $publish_up;

    /**
     *
     * @var string
     */
    public $publish_down;

    /**
     *
     * @var string
     */
    public $images;

    /**
     *
     * @var string
     */
    public $urls;

    /**
     *
     * @var string
     */
    public $attribs;

    /**
     *
     * @var integer
     */
    public $version;

    /**
     *
     * @var integer
     */
    public $ordering;

    /**
     *
     * @var string
     */
    public $metakey;

    /**
     *
     * @var string
     */
    public $metadesc;

    /**
     *
     * @var integer
     */
    public $access;

    /**
     *
     * @var integer
     */
    public $hits;

    /**
     *
     * @var string
     */
    public $metadata;

    /**
     *
     * @var integer
     */
    public $featured;

    /**
     *
     * @var string
     */
    public $language;

    /**
     *
     * @var string
     */
    public $xreference;

    public function createAlias($title) {
        $filter = new Filter();
        return $filter->sanitize($title, 'alphanum');
    }

    public function getHighestOrder() {
        $article = Article::find(
            [
                "order" => "ordering DESC",
                "limit" => 1,
                "columns" => "ordering",
            ]
        );
        return $article->ordering;
    }

    /**
     * @return Article[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return Article
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id' => 'id',
            'asset_id'  => 'asset_id',
            'title' => 'title',
            'alias' => 'alias',
            'introtext' => 'introtext',
            'fulltext'  => 'fulltext',
            'state' => 'state',
            'catid' => 'catid',
            'created' => 'created',
            'created_by' => 'created_by',
            'created_by_alias' => 'created_by_alias',
            'modified'  => 'modified',
            'modified_by' => 'modified_by',
            'checked_out' => 'checked_out',
            'checked_out_time' => 'checked_out_time',
            'publish_up' => 'publish_up',
            'publish_down' => 'publish_down',
            'images' => 'images',
            'urls' => 'urls',
            'attribs' => 'attribs',
            'version' => 'version',
            'ordering' => 'ordering',
            'metakey' => 'metakey',
            'metadesc' => 'metadesc',
            'access' => 'access',
            'hits' => 'hits',
            'metadata' => 'metadata',
            'featured' => 'featured',
            'language' => 'language',
            'xreference' => 'xreference'
        );
    }

    public function initialize()
    {
        $this->setConnectionService("school_db");
        $this->setSource("mdqhk_content");
        $this->skipAttributes(
            [
                'fulltext',
                'metakey',
                'metadesc',
                "xreference",
            ]
        );
    }

    public function beforeValidationOnCreate() {
        $created = new DateTime();
        $created->setTimezone(new DateTimeZone("UTC"));
        $this->created = $created->format("Y-m-d H:i:s");
        $this->publish_up = $created->format("Y-m-d H:i:s");
        $this->alias = $this->createAlias($this->title);
        $this->created_by_alias = 'Infos';
        $this->featured = 1;
        $this->version = 1;
        $this->access = 1;
        $this->state = 1;
        $this->language = '*';
        $this->metadata = '{"robots":"","author":"","rights":"","xreference":""}';
        $this->attribs = '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","info_block_show_title":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":"","extra-class":""}';
        $this->urls = '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}';
        $this->images = '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}';
    }

    public function beforeValidationOnUpdate() {
        $this->alias = $this->createAlias($this->title);
        $updated = new DateTime();
        $updated->setTimezone(new DateTimeZone("UTC"));
        $this->modified = $updated->format("Y-m-d H:i:s");
        $this->created_by_alias = 'Infos';
    }

}
