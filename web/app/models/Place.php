<?php

use Phalcon\Mvc\Model;

class Place extends Model {

    /**
     *
     * @var integer
     */
    public $id_place;

    /**
     *
     * @var string
     */
    public $plc_name;

    /**
     * @return Place[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return Place
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_place' => 'id_place', 
            'plc_name' => 'plc_name'
        );
    }

}
