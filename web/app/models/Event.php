<?php

use Phalcon\Mvc\Model;

class Event extends Model {

    /**
     *
     * @var integer
     */
    public $id_event;

    /**
     *
     * @var string
     */
    public $id_user;

    /**
     *
     * @var integer
     */
    public $id_eventType;

    /**
     *
     * @var string
     */
    public $evt_created;

    /**
     *
     * @var string
     */
    public $evt_updated;

    /**
     *
     * @var string
     */
    public $evt_date;

    /**
     *
     * @var string
     */
    public $evt_startTime;

    /**
     *
     * @var string
     */
    public $evt_endTime;

	/**
	 * @var string
	 */
	public $evt_title;

	/**
	 * @var string
	 */
	public $evt_place;

    /**
     *
     * @var string
     */
    public $evt_description;

    /**
     *
     * @var string
     */
    public $evt_shortDescription;

    /**
     *
     * @var string
     */
    public $evt_program;

    /**
     *
     * @var boolean
     */
    public $evt_public;

    /**
     *
     * @var boolean
     */
    public $evt_google = false;

    /**
     *
     * @var string
     */
    public $evt_uid;

    /**
     *
     * @var boolean
     */
    public $evt_infos;

    /**
     * @return Event[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return Event
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_event' => 'id_event', 
            'id_user' => 'id_user', 
            'id_eventType' => 'id_eventType', 
            'evt_created' => 'evt_created', 
            'evt_updated' => 'evt_updated', 
            'evt_date' => 'evt_date', 
            'evt_startTime' => 'evt_startTime', 
            'evt_endTime' => 'evt_endTime',
			'evt_title' => 'evt_title',
			'evt_place' => 'evt_place',
            'evt_description' => 'evt_description',
            'evt_shortDescription' => 'evt_shortDescription',
            'evt_program' => 'evt_program',
            'evt_public' => 'evt_public',
            'evt_google' => 'evt_google',
            'evt_uid' => 'evt_uid',
            'evt_infos' => 'evt_infos'
        );
    }

	public function initialize() {
		$this->belongsTo('id_user', 'User', 'id_user', array(
			'alias' => 'User'
		));
		$this->belongsTo('id_eventType', 'EventType', 'id_eventType', array(
			'alias' => 'EventType'
		));
		$this->hasMany('id_event', 'EventGoogle', 'id_event', array(
			'alias' => 'EventGoogle'
		));
	}

}
