<?php

use Phalcon\Mvc\Model;

class User extends Model {

    /**
     *
     * @var string
     */
    public $id_user;

    /**
     *
     * @var integer
     */
    public $id_role;

    /**
     *
     * @var string
     */
    public $usr_nick;

    /**
     *
     * @var string
     */
    public $usr_email;

    /**
     *
     * @var string
     */
    public $usr_firstName;

    /**
     *
     * @var string
     */
    public $usr_lastName;

    /**
     *
     * @var string
     */
    public $usr_calendarSuplo;

    /**
     *
     * @var string
     */
    public $usr_viewName;

    /**
     *
     * @var integer
     */
    public $usr_google;

    /**
     * @return User[]
     */
    public static function find($parameters = array())
    {
        return parent::find($parameters);
    }

    /**
     * @return User
     */
    public static function findFirst($parameters = array())
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id_user' => 'id_user', 
            'id_role' => 'id_role', 
            'usr_nick' => 'usr_nick', 
            'usr_email' => 'usr_email', 
            'usr_firstName' => 'usr_firstName', 
            'usr_lastName' => 'usr_lastName', 
            'usr_calendarSuplo' => 'usr_calendarSuplo', 
            'usr_viewName' => 'usr_viewName',
            'usr_google' => 'usr_google'
        );
    }

	public function initialize() {
		$this->belongsTo('id_role', 'Role', 'id_role', array(
			'alias' => 'Role'
		));
		$this->hasMany('id_user', 'Announcement', 'id_user', array(
			'alias' => 'Announcement'
		));
	}

	public function getFullName() {
		return $this->usr_firstName . " " . $this->usr_lastName;
	}

}
