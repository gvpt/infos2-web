<?php

use Phalcon\Mvc\Model;

class Suplo extends Model {

    /**
     *
     * @var integer
     */
    public $id_suplo;

    /**
     *
     * @var string
     */
    public $id_user;

	/**
	 * @var integer
	 */
	public $id_timetable;

    /**
     *
     * @var string
     */
    public $sup_nick;

    /**
     *
     * @var string
     */
    public $sup_date;

    /**
     *
     * @var string
     */
    public $sup_classes;

    /**
     *
     * @var string
     */
    public $sup_note;

    /**
     *
     * @var string
     */
    public $sup_classroom;

    /**
     *
     * @var string
     */
    public $sup_subject;

    /**
     *
     * @var string
     */
    public $sup_eventId;

    /**
     * @return Suplo[]
     */
    public static function find($parameters = []) {
        return parent::find($parameters);
    }

    /**
     * @return Suplo
     */
    public static function findFirst($parameters = []) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_suplo' => 'id_suplo',
			'id_timetable' => 'id_timetable',
            'id_user' => 'id_user', 
            'sup_nick' => 'sup_nick', 
            'sup_date' => 'sup_date',
            'sup_classes' => 'sup_classes', 
            'sup_note' => 'sup_note', 
            'sup_classroom' => 'sup_classroom', 
            'sup_subject' => 'sup_subject', 
            'sup_eventId' => 'sup_eventId'
        );
    }

	public function initialize() {
		$this->belongsTo('id_user', 'User', 'id_user', array(
			'alias' => 'User'
		));
		$this->belongsTo('sup_nick', 'User', 'usr_nick', array(
			'alias' => 'Missing'
		));
		$this->belongsTo('id_timetable', 'Timetable', 'id_timetable', array(
			'alias' => 'Hour'
		));
	}

	public function setUser($displayName) {
		$sql = "SELECT id_user FROM user WHERE usr_viewName = ?";
		/**
		 * @var $db \Phalcon\Db\Adapter\Pdo\MySQL
		 */
		$db = $this->getReadConnection();
		try {
			$result = $db->fetchOne($sql,Phalcon\Db::FETCH_ASSOC, [$displayName]);
			$this->id_user = $result['id_user'];
			return true;
		}
		catch (Exception $e) {
			$this->getDI()->getLogger()->error("User " . $this->getDI()->getAuth()->getName() . " has failed to create substitution for " . $this->sup_date . " - unable to set id_user for missing teacher. \n" . $e->getMessage());
			return false;
		}
	}

	public function setHour($hour) {
		$sql = "SELECT id_timetable FROM timetable WHERE tmt_lesson = ?";
		/**
		 * @var $db \Phalcon\Db\Adapter\Pdo\MySQL
		 */
		$db = $this->getReadConnection();
		try {
			$result = $db->fetchOne($sql,Phalcon\Db::FETCH_ASSOC, [$hour]);
			$this->id_timetable = $result['id_timetable'];
			return true;
		}
		catch (Exception $e) {
			$this->getDI()->getLogger()->error("User " . $this->getDI()->getAuth()->getName() . " has failed to create substitution for " . $this->sup_date . " - unable to set id_timetable. \n" . $e->getMessage());
			return false;
		}
	}

	public function getClassesShort() {
		$result = substr($this->sup_classes, 0, 3);
		if (strlen($this->sup_classes) > 3) {
			return $result . ' ...';
		}
		else {
			return $result;
		}
	}

}
