<?php

use Phalcon\Mvc\Model;

class EventGoogle extends Model {

    /**
     *
     * @var string
     */
    public $id_eventGoogle;

    /**
     *
     * @var integer
     */
    public $id_event;

    /**
     *
     * @var string
     */
    public $id_user;

    /**
     * @return EventGoogle[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return EventGoogle
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource('eventGoogle');
    	$this->belongsTo('id_event', 'Event', 'id_event', array(
			'alias' => 'Event'
		));
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id_eventGoogle' => 'id_eventGoogle', 
            'id_event' => 'id_event', 
            'id_user' => 'id_user'
        );
    }

}