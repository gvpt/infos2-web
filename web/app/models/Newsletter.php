<?php

use Phalcon\Mvc\Model;

class Newsletter extends Model {

    /**
     *
     * @var integer
     */
    public $id_newsletter;

    /**
     *
     * @var string
     */
    public $id_user;

    /**
     *
     * @var string
     */
    public $nws_email;

    /**
     *
     * @var integer
     */
    public $nws_announcements;

    /**
     *
     * @var integer
     */
    public $nws_events;

    /**
     *
     * @var string
     */
    public $nws_suplo = 'NONE';

    /**
     * @return Newsletter[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return Newsletter
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_newsletter' => 'id_newsletter', 
            'id_user' => 'id_user', 
            'nws_email' => 'nws_email', 
            'nws_announcements' => 'nws_announcements', 
            'nws_events' => 'nws_events', 
            'nws_suplo' => 'nws_suplo'
        );
    }

	public function initialize() {
		$this->belongsTo('id_user', 'User', 'id_user', [
			'alias' => 'User'
		]);
	}

	public function setSuplo($value) {
		$this->nws_suplo = strtoupper($value);
	}
}
