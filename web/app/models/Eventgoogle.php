<?php

use Phalcon\Mvc\Model;

class EventGoogle extends Model {

    /**
     *
     * @var string
     */
    public $id_eventGoogle;

    /**
     *
     * @var integer
     */
    public $id_event;

    /**
     *
     * @var string
     */
    public $id_user;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource('eventGoogle');
    	$this->belongsTo('id_event', 'Event', 'id_event', array(
			'alias' => 'Event'
		));
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id_eventGoogle' => 'id_eventGoogle', 
            'id_event' => 'id_event', 
            'id_user' => 'id_user'
        );
    }

}