<?php

use Phalcon\Mvc\Model;

class SystemMessage extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     */
    public $id_systemMessage;

    /**
     *
     * @var string
     */
    public $id_user;

    /**
     *
     * @var string
     */
    public $sysm_created;

    /**
     *
     * @var string
     */
    public $sysm_updated;

    /**
     *
     * @var string
     */
    public $sysm_title;

    /**
     *
     * @var string
     */
    public $sysm_text;

    /**
     *
     * @var string
     */
    public $sysm_deadline;

    /**
     *
     * @var integer
     */
    public $sysm_type;

    /**
     *
     * @var integer
     */
    public $sysm_active;

    /**
     * @return SystemMessage[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return SystemMessage
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_systemMessage' => 'id_systemMessage', 
            'id_user' => 'id_user', 
            'sysm_created' => 'sysm_created', 
            'sysm_updated' => 'sysm_updated', 
            'sysm_title' => 'sysm_title', 
            'sysm_text' => 'sysm_text', 
            'sysm_deadline' => 'sysm_deadline',
            'sysm_type' => 'sysm_type',
            'sysm_active' => 'sysm_active'
        );
    }

	public function initialize() {
        $this->setSource('systemMessage');
        $this->belongsTo('id_user', 'User', 'id_user', [
			'alias' => 'User'
		]);
	}

}
