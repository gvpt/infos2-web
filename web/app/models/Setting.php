<?php

use Phalcon\Mvc\Model;

class Setting extends Model {

    /**
     *
     * @var integer
     */
    public $id_setting;

    /**
     *
     * @var string
     */
    public $id_user;

    /**
     *
     * @var integer
     */
    public $set_logoutGoogle;


    /**
     * @return Setting[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return Setting
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_setting' => 'id_setting',
            'id_user' => 'id_user',
            'set_logoutGoogle' => 'set_logoutGoogle'
        );
    }

    public function initialize() {
        $this->belongsTo('id_user', 'User', 'id_user', [
            'alias' => 'User'
        ]);
    }

}
