<?php

use Phalcon\Mvc\Model;

class Permission extends Model
{

    /**
     *
     * @var integer
     */
    public $id_permission;

    /**
     *
     * @var integer
     */
    public $id_role;

    /**
     *
     * @var string
     */
    public $per_resource;

    /**
     *
     * @var string
     */
    public $per_action;

    /**
     * @return Permission[]
     */
    public static function find($parameters = [])
    {
        return parent::find($parameters);
    }

    /**
     * @return Permission
     */
    public static function findFirst($parameters = [])
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_permission' => 'id_permission', 
            'id_role' => 'id_role', 
            'per_resource' => 'per_resource', 
            'per_action' => 'per_action'
        );
    }

	public function initialize() {
		$this->belongsTo('id_role', 'Role', 'id_role', array(
			'alias' => 'Role'
		));
	}

}
