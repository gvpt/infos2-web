<?php

use Phalcon\Mvc\Model;

class EventType extends Model {

    /**
     *
     * @var integer
     */
    public $id_eventType;

    /**
     *
     * @var string
     */
    public $evy_type;

    /**
     *
     * @var string
     */
    public $evy_label;

    /**
     * @return EventType[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return EventType
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource('eventType');
    	$this->hasMany('id_event', 'Event', 'id_event', array(
			'alias' => 'Event'
		));
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_eventType' => 'id_eventType', 
            'evy_type' => 'evy_type', 
            'evy_label' => 'evy_label'
        );
    }

}