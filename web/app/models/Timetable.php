<?php

class Timetable extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     */
    public $id_timetable;

    /**
     *
     * @var integer
     */
    public $tmt_lesson;

    /**
     *
     * @var string
     */
    public $tmt_startTime;

    /**
     *
     * @var string
     */
    public $tmt_endTime;

    /**
     *
     * @var string
     */
    public $tmt_label;

    /**
     * @return Timetable[]
     */
    public static function find($parameters = array()) {
        return parent::find($parameters);
    }

    /**
     * @return Timetable
     */
    public static function findFirst($parameters = array()) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_timetable' => 'id_timetable', 
            'tmt_lesson' => 'tmt_lesson', 
            'tmt_startTime' => 'tmt_startTime', 
            'tmt_endTime' => 'tmt_endTime', 
            'tmt_label' => 'tmt_label'
        );
    }

}
