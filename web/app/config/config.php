<?php

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'  => 'Mysql',
        'host'     => getenv('DATABASE_HOST'),
        'username' => getenv('DATABASE_USER'),
        'password' => getenv('DATABASE_PASS'),
        'dbname'   => getenv('DATABASE_NAME')
    ),
    'school_db' => array(
        'adapter'   => 'Mysql',
        'host'      => getenv('SCHOOL_DATABASE_HOST'),
        'username'  => getenv('SCHOOL_DATABASE_USER'),
        'password'  => getenv('SCHOOL_DATABASE_PASS'),
        'dbname'    => getenv('SCHOOL_DATABASE_NAME')
    ),
    'application' => array(
        'controllersDir'	=> __DIR__ . '/../../app/controllers/',
		'formsDir' 			=> __DIR__ . '/../../app/forms/',
        'modelsDir'			=> __DIR__ . '/../../app/models/',
        'viewsDir'			=> __DIR__ . '/../../app/views/',
        'pluginsDir'		=> __DIR__ . '/../../app/plugins/',
        'libraryDir'		=> __DIR__ . '/../../app/library/',
        'cacheDir'			=> __DIR__ . '/../../app/cache/',
        'baseUri'			=> getenv('BASE_URI'),
        'catId'             => 72
    ),
	'metadata' => array(
		'adapter' 		=> 'Apc',
		'suffix' 		=> 'my-suffix',
		'livetime' 		=> '3600'
	),
	'google' => array(
		'clientId' => getenv('GOOGLE_CLIENT_ID'),
		'clientSecret' => getenv('GOOGLE_CLIENT_SECRET')
	)
));
