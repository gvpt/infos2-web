<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as Flash;
use Phalcon\Logger;
use Phalcon\Events\Manager as EventManager;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Events\Manager as EventsManager;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * We register the events manager
 */
$di->set('dispatcher', function () use ($di) {
	$eventsManager = new EventsManager;

	/**
	 * Handle exceptions and not-found exceptions using NotFoundPlugin
	 */
	$eventsManager->attach('dispatch:beforeException', new NotFoundPlugin);
	
	$dispatcher = new Dispatcher;
	$dispatcher->setEventsManager($eventsManager);
	return $dispatcher;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
		'.volt' => function ($view, $di) use ($config) {
			$volt = new VoltEngine($view, $di);
			$volt->setOptions(array(
				'compiledPath' => $config->application->cacheDir . 'volt/',
                'compiledSeparator' => '_',
				//'compileAlways' => true
            ));
			$volt->getCompiler()->addFunction('strtotime', 'strtotime');
			return $volt;
        },

		'.phtml' => 'Phalcon\Mvc\View\Engine\Php',
    ));

    return $view;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('school_db', function () use ($config) {

	$eventsManager = new EventManager();

	$log = new FileLogger(APP_DIR . "/logs/school_db.log");

	$connection =  new DbAdapter(array(
		'host' => $config->school_db->host,
		'username' => $config->school_db->username,
		'password' => $config->school_db->password,
		'dbname' => $config->school_db->dbname,
		"options" => array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
		)
	));

	/**
	 * $event EventType
	 */
	$eventsManager->attach('school_db', function($event, $connection) use ($log) {
		if ($event->getType() == 'beforeQuery') {
			$log->log($connection->getSQLStatement(), Logger::INFO);
		}
	});
	$connection->setEventsManager($eventsManager);

	return $connection;

});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {

    $eventsManager = new EventManager();

    $log = new FileLogger(APP_DIR . "/logs/db.log");

    $connection =  new DbAdapter(array(
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        "options" => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));

    /**
     * $event EventType
     */
    $eventsManager->attach('db', function($event, $connection) use ($log) {
        if ($event->getType() == 'beforeQuery') {
            $log->log($connection->getSQLStatement(), Logger::INFO);
        }
    });
    $connection->setEventsManager($eventsManager);

    return $connection;

});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di->set('config', $config);

$di->set('auth', function () {
	return new Auth();
});

$di->set('acl', function () {
	return new Acl();
});

$di->set('flash', function () {
	return new Flash(array(
		'error' => 'alert-box alert',
		'success' => 'alert-box success',
		'notice' => 'alert-box info'
	));
});

$di->set('logger', function() use ($config) {

	$connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
		'host' => $config->database->host,
		'username' => $config->database->username,
		'password' => $config->database->password,
		'dbname' => $config->database->dbname,
		"options" => array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
		)
	));

	$logger = new DatabaseLogger(array(
		'db' => $connection,
		'table' => 'log'
	));

	return $logger;
});