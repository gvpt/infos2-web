{% extends "layouts/base.volt" %}
{% block title %}{{ event.evt_title }}{% endblock %}
{% block content %}
	<div class="row">
		<div class="large-12 columns">
			<article>
				<header>
					<h1><img src="{{ url("images/events/") }}{{ event.getEventType().evy_type }}_32.png" alt="{{ event.getEventType().evy_label }}" title="{{ event.getEventType().evy_label }}" style="margin-right: 10px; margin-bottom: 5px;">{{ event.evt_title }}</h1>
					<ul class="inline-list">
						<li><span class="fi-info"> {{ event.getEventType().evy_label }}</span></li>
						<li><span class="fi-calendar"> {{ date("j. n. Y", strtotime(event.evt_date)) }}</span></li>
						<li><span class="fi-clock">
								{% if event.evt_startTime %}
                                    {{ date("H:i", strtotime(event.evt_startTime)) }}
                                {% else %}
									Neurčené
                                {% endif %}
								-
                                {% if event.evt_startTime %}
                                    {{ date("H:i", strtotime(event.evt_endTime)) }}
                                {% else %}
									Neurčené
                                {% endif %}
							</span></li>
						<li><span class="fi-map"> {{ event.evt_place }}</span></li>
						{% if auth.isAdmin() %}
							<li> | </li>
							<li><a class="fi-pencil" href="{{ url("events/edit/") }}{{ event.id_event }}"> Upraviť</a> </li>
							<li><a class="fi-trash" href="{{ url("events/delete/") }}{{ event.id_event }}"> Odstrániť</a> </li>
						{% endif %}
					</ul>
				</header>
				{% if event.evt_program %}
					<h2>Program</h2>
					{{ event.evt_program }}
				{% endif %}
				<h2>Popis</h2>
				{{ event.evt_description }}
				<small><a href="https://plus.google.com/u/1/{{ auth.getIdentity()['id'] }}/about" target="_blank" class="fi-torso"> {{ event.getUser().usr_firstName }} {{ event.getUser().usr_lastName }}</a> - <time pubdate="{{ event.evt_created }}">{{ date("j. n. Y H:i", strtotime(event.evt_created)) }}</time></small>
			</article>
		</div>
	</div>
{% endblock %}