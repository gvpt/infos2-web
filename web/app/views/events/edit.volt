{% extends "layouts/base.volt" %}
{% block title %}Upraviť udalosť{% endblock %}
{% block content %}
	<section class="row">
		<div class="large-12 columns">
			<h2><img src="{{ url("images/events/") }}{{ event.getEventType().evy_type }}_32.png" alt="{{ event.getEventType().evy_label }}" title="{{ event.getEventType().evy_label }}" style="margin-right: 10px; margin-bottom: 5px;">{{ event.evt_title }}</h2>
			<form id="formEditEvent" name="formEditEvent" action="{{ url("events/edit/") }}{{ event.id_event }}" enctype="application/x-www-form-urlencoded" method="post" data-abide>
				<div class="row">
					<div class="large-6 columns">
						<label>Názov
							<input type="text" id="editEvent_title" name="editEvent_title" placeholder="Názov udalosti" required value="{{ event.evt_title }}"/>
						</label>
						<small class="error">Udalosť musí mať názov!</small>
					</div>
				</div>
				<div class="row">
					<div class="large-4 columns">
						<label>Dátum
							<input type="text" id="editEvent_date" name="editEvent_date" placeholder="Dátum" value="{{ date("d.m.Y", strtotime(event.evt_date)) }}" required pattern="(0[1-9]|[12][0-9]|3[01])[- \.](0[1-9]|1[012])[- \.](19|20)\d\d"/>
							<small class="error">Položka musí obsahovať validný dátum.</small>
						</label>
					</div>
					<div class="large-4 columns">
						<label>Začiatok
							<input type="text" id="editEvent_start" name="editEvent_start" placeholder="Začiatok" pattern="(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9])"
                                   {% if event.evt_startTime %}value="{{ date("H:i", strtotime(event.evt_startTime)) }}"{% endif %}
								   {% if eventType.evy_type == "meeting" or eventType.evy_type == "ribbon" %}required{% endif %} />
							<small class="error">Položka musí obsahovať validný čas.</small>
						</label>
					</div>
					<div class="large-4 columns">
						{% if event.getEventType().evy_type != 'ribbon' %}
							<label>Koniec
								<input type="text" id="editEvent_end" name="editEvent_end" placeholder="Koniec" pattern="(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9])"
                                       {% if event.evt_endTime %}value="{{ date("H:i", strtotime(event.evt_endTime)) }}"{% endif %}
									   {% if eventType.evy_type == "meeting" %}required{% endif %} />
								<small class="error">Položka musí obsahovať validný čas.</small>
							</label>
						{% endif %}
					</div>
				</div>
				<div class="row">
					<div class="large-6 columns">
						<label>Kde
							<input type="text" list="places" id="editEvent_place" name="editEvent_place" placeholder="Miesto/Trieda" value="{{ event.evt_place }}"
                                   {% if eventType.evy_type == "meeting" or eventType.evy_type == "ribbon" %}required{% endif %}/>
						</label>
						<small class="error">Udalosť musí mať miesto konania!</small>
					</div>
				</div>
				<datalist id="places">
					{% for place in places %}
					<option value="{{ place.plc_name }}">
						{% endfor %}
				</datalist>
				<div class="row">
					<div class="large-2 columns">
						<label for="newEvent_program">Verejna akcia.
							<input type="checkbox" id="editEvent_public" name="editEvent_public" {% if event.evt_public %}checked{% endif %}>
						</label>
					</div>
					<div class="large-2 columns">
						<label for="newEvent_program">Akcia v infose.
							<input type="checkbox" id="editEvent_infos" name="editEvent_infos" {% if event.evt_infos %}checked{% endif %}>
						</label>
					</div>
					<div class="large-6 columns {% if not event.evt_public %}hide-for-small-up{% endif %}" id="shortDescription">
						<label>Kratky popis
							<input type="text" id="editEvent_shortDescription" name="editEvent_shortDescription" value="{{ event.evt_shortDescription }}" placeholder="Kratky popis" maxlength="60"/>
						</label>
						<small class="error">Kratky popis nemoze mat viac ako 60 znakov!</small>
					</div>
				</div>
				{% if event.getEventType().evy_type == 'meeting' %}
					<div class="row">
						<div class="large-12 columns">
							<label for="editEvent_program">Program</label>
							<textarea id="editEvent_program" name="editEvent_program" style="height: 400px; width: 100%;">{{ event.evt_program }}</textarea>
						</div>
					</div>
				{% endif %}
				<div class="row">
					<div class="large-12 columns">
						<label for="editEvent_description">Poznámka</label>
						<textarea id="editEvent_description" name="editEvent_description" style="height: 400px; width: 100%;">{{ event.evt_description }}</textarea>
					</div>
				</div>
				<div class="row">
					<div class="large-5 large-centered columns">
						<input type="hidden" value="{{ event.getEventType().id_eventType }}" name="editEvent_type" id="editEvent_type" />
						<button type="submit" style="margin-top: 20px;" class="button expand">Odoslať</button>
					</div>
				</div>
			</form>
		</div>
	</section>
{% endblock %}
{% block javascript %}
	<script type="text/javascript">
		$(document).ready(function() {
			$("#editEvent_date").datepicker({
				dateFormat: "dd.mm.yy",
				firstDay: 1,
				dayNamesMin:["Ne", "Po", "Ut", "St", "Št", "Pia", "So"],
				monthNames: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
				minDate: 0
			});
			tinymce.init({
				mode: "textareas",
				language : 'sk',
				plugins: ["autolink link charmap anchor hr","searchreplace visualblocks code ","table paste autoresize media image pagebreak"],
				toolbar: "styleselect edit | bold italic underline strikethrough superscript subscript | removeformat | link image media pagebreak hr | table | code",
				entity_encoding : "raw",
				width: "100%",
				menubar: false,
				style_formats: [
					{title: "Headers", items: [
						{title: "Header 1", format: "h1"},
						{title: "Header 2", format: "h2"},
						{title: "Header 3", format: "h3"},
						{title: "Header 4", format: "h4"},
						{title: "Header 5", format: "h5"},
						{title: "Header 6", format: "h6"}
					]},
					{title: "Inline", items: [
						{title: "Bold", icon: "bold", format: "bold"},
						{title: "Italic", icon: "italic", format: "italic"},
						{title: "Underline", icon: "underline", format: "underline"},
						{title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
						{title: "Superscript", icon: "superscript", format: "superscript"},
						{title: "Subscript", icon: "subscript", format: "subscript"},
						{title: "Code", icon: "code", format: "code"}
					]},
					{title: "Blocks", items: [
						{title: "Paragraph", format: "p"},
						{title: "Blockquote", format: "blockquote"},
						{title: "Div", format: "div"},
						{title: "Pre", format: "pre"}
					]},
					{title: "Alignment", items: [
						{title: "Left", icon: "alignleft", format: "alignleft"},
						{title: "Center", icon: "aligncenter", format: "aligncenter"},
						{title: "Right", icon: "alignright", format: "alignright"},
						{title: "Justify", icon: "alignjustify", format: "alignjustify"}
					]}
				]
			});
			$('#editEvent_start').clockpicker({
				autoclose: 'true'
			});
			{% if event.getEventType().evy_type != 'ribbon' %}
			$('#editEvent_end').clockpicker({
				autoclose: 'true'
			});
			{% endif %}
		});
        $('#editEvent_public').click(function() {
            var inpt = document.getElementById('editEvent_shortDescription');
            if (this.checked) {
                $('#shortDescription').removeClass('hide-for-small-up');
                inpt.required = true;
            } else {
                inpt.required = false;
                var el = document.getElementById('shortDescription');
                if (!el.classList.contains("hide-for-small-up")) {
                    $('#shortDescription').addClass('hide-for-small-up');
                }
            }
        });
	</script>
{% endblock %}