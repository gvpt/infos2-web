{% extends "layouts/base.volt" %}
{% block title %}Udalosti - {{ offset }}{% endblock %}
{% block content %}
	<div class="row">
		<div class="large-12 columns">
			<table style="width: 100%;">
				<tr>
					<th>Typ</th>
					<th>Názov</th>
					<th>Miesto</th>
					<th>Dátum konania</th>
					<th>Začiatok</th>
					<th>Koniec</th>
				</tr>
				{% for event in pagination.items %}
					<tr data-url="{{ url("events/view/") }}{{ event.id_event }}">
						<td class="text-center"><img src="{{ url("images/events/") }}{{ event.getEventType().evy_type }}_32.png" alt="{{ event.getEventType().evy_label }}" title="{{ event.getEventType().evy_label }}"></td>
						<td>{{ event.evt_title }}</td>
						<td>{{ event.evt_place }}</td>
						<td>{{ date("j. n. Y", strtotime(event.evt_date)) }}</td>
						<td>
							{% if event.evt_startTime %}
								{{ date("H:i", strtotime(event.evt_startTime)) }}
							{% else %}
								Neurčené
							{% endif %}
						</td>
						<td>
                            {% if event.evt_startTime %}
                                {{ date("H:i", strtotime(event.evt_endTime)) }}
                            {% else %}
								Neurčené
                            {% endif %}
						</td>
					</tr>
				{% endfor %}
			</table>
			<div class="pagination-centered">
				<ul class="pagination">
					{% if pagination.current == pagination.first %}
						<li class="arrow unavailable"><a href="{{ url("events/list/") }}{{ pagination.current }}">&laquo;</a></li>
					{% else %}
						<li class="arrow"><a href="{{ url("events/list/") }}{{ pagination.before }}">&laquo;</a></li>
					{% endif %}
					{% for index in 1..pagination.total_pages %}
						{% if index == pagination.current %}
							<li class="current"><a href="{{ url("events/list/") }}{{ index }}">{{ index }}</a></li>
						{% else %}
							<li><a href="{{ url("events/list/") }}{{ index }}">{{ index }}</a></li>
						{% endif %}
					{% endfor %}
					{% if pagination.current == pagination.last %}
						<li class="arrow unavailable"><a href="{{ url("events/list/") }}{{ pagination.current }}">&raquo;</a></li>
					{% else %}
						<li class="arrow"><a href="{{ url("events/list/") }}{{ pagination.next }}">&raquo;</a></li>
					{% endif %}
				</ul>
			</div>
		</div>
	</div>
{% endblock %}