{% extends "layouts/base.volt" %}
{% block title %}Nová udalosť{% endblock %}
{% block content %}
	<section class="row">
		<div class="large-12 columns">
			<header><h2>{{ eventType.evy_label }}</h2></header>
			<form id="formNewEvent" name="formNewEvent" action="{{ url("events/new/") }}{{ eventType.evy_type }}" enctype="application/x-www-form-urlencoded" method="post" data-abide>
				<div class="row">
					<div class="large-6 columns">
						<label>Názov
							<input type="text" id="newEvent_title" name="newEvent_title" placeholder="Názov udalosti" required/>
						</label>
						<small class="error">Udalosť musí mať názov!</small>
					</div>
				</div>
				<div class="row">
					<div class="large-4 columns">
						<label>Dátum
							<input type="text" id="newEvent_date" name="newEvent_date" placeholder="Dátum" required pattern="(0[1-9]|[12][0-9]|3[01])[- \.](0[1-9]|1[012])[- \.](19|20)\d\d"/>
							<small class="error">Položka musí obsahovať validný dátum.</small>
						</label>
					</div>
					<div class="large-4 columns">
						<label>Začiatok
							<input type="text" id="newEvent_start" name="newEvent_start" placeholder="Začiatok" pattern="(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9])"
								{% if eventType.evy_type == "meeting" or eventType.evy_type == "ribbon" %}required{% endif %} />
							<small class="error">Položka musí obsahovať validný čas.</small>
						</label>
					</div>
					<div class="large-4 columns">
						{% if endTime is defined %}
							<label>Koniec
								<input type="text" id="newEvent_end" name="newEvent_end" placeholder="Koniec" pattern="(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9])"
									   {% if eventType.evy_type == "meeting" %}required{% endif %} />
								<small class="error">Položka musí obsahovať validný čas.</small>
							</label>
						{% endif %}
					</div>
				</div>
				<div class="row">
					<div class="large-6 columns">
						<label>Kde
							<input type="text" list="places" id="newEvent_place" name="newEvent_place" placeholder="Miesto/Trieda"
                                   {% if eventType.evy_type == "meeting" or eventType.evy_type == "ribbon" %}required{% endif %} />
						</label>
						<small class="error">Udalosť musí mať miesto konania!</small>
					</div>
				</div>
				<datalist id="places">
					{% for place in places %}
					<option value="{{ place.plc_name }}">
					{% endfor %}
				</datalist>
                <div class="row">
                    <div class="large-2 columns">
                        <label for="newEvent_public">Verejna akcia.
                            <input type="checkbox" id="newEvent_public" name="newEvent_public">
                        </label>
                    </div>
                    <div class="large-2 columns">
                        <label for="newEvent_infos">Akcia v infose.
                            <input type="checkbox" id="newEvent_infos" name="newEvent_infos">
                        </label>
                    </div>
                    <div class="large-6 columns hide-for-small-up" id="shortDescription">
                        <label>Kratky popis
                            <input type="text" id="newEvent_shortDescription" name="newEvent_shortDescription" placeholder="Kratky popis" maxlength="60"/>
                        </label>
                        <small class="error">Kratky popis nemoze mat viac ako 60 znakov!</small>
                    </div>
                </div>
				{% if program is defined %}
					<div class="row">
						<div class="large-12 columns">
							<label for="newEvent_program">Program</label>
							<textarea id="newEvent_program" name="newEvent_program" style="height: 400px; width: 100%;"></textarea>
						</div>
					</div>
				{% endif %}
				<div class="row">
					<div class="large-12 columns">
						<label for="newEvent_description">Poznámka</label>
						<textarea id="newEvent_description" name="newEvent_description" style="height: 400px; width: 100%;"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="large-5 large-centered columns">
						<input type="hidden" value="{{ eventType.id_eventType }}" name="newEvent_type" id="newEvent_type" />
						<button type="submit" style="margin-top: 20px;" class="button expand">Odoslať</button>
					</div>
				</div>
			</form>
		</div>
	</section>
{% endblock %}
{% block javascript %}
	<script type="text/javascript">
		$(document).ready(function() {
			$("#newEvent_date").datepicker({
				dateFormat: "dd.mm.yy",
				firstDay: 1,
				dayNamesMin:["Ne", "Po", "Ut", "St", "Št", "Pia", "So"],
				monthNames: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
				minDate: 0
			});
			tinymce.init({
				mode: "textareas",
				language : 'sk',
				plugins: ["autolink link charmap anchor hr","searchreplace visualblocks code ","table paste autoresize media image pagebreak"],
				toolbar: "styleselect edit | bold italic underline strikethrough superscript subscript | removeformat | link image media pagebreak hr | table | code",
				entity_encoding : "raw",
				width: "100%",
				menubar: false,
				style_formats: [
					{title: "Headers", items: [
						{title: "Header 1", format: "h1"},
						{title: "Header 2", format: "h2"},
						{title: "Header 3", format: "h3"},
						{title: "Header 4", format: "h4"},
						{title: "Header 5", format: "h5"},
						{title: "Header 6", format: "h6"}
					]},
					{title: "Inline", items: [
						{title: "Bold", icon: "bold", format: "bold"},
						{title: "Italic", icon: "italic", format: "italic"},
						{title: "Underline", icon: "underline", format: "underline"},
						{title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
						{title: "Superscript", icon: "superscript", format: "superscript"},
						{title: "Subscript", icon: "subscript", format: "subscript"},
						{title: "Code", icon: "code", format: "code"}
					]},
					{title: "Blocks", items: [
						{title: "Paragraph", format: "p"},
						{title: "Blockquote", format: "blockquote"},
						{title: "Div", format: "div"},
						{title: "Pre", format: "pre"}
					]},
					{title: "Alignment", items: [
						{title: "Left", icon: "alignleft", format: "alignleft"},
						{title: "Center", icon: "aligncenter", format: "aligncenter"},
						{title: "Right", icon: "alignright", format: "alignright"},
						{title: "Justify", icon: "alignjustify", format: "alignjustify"}
					]}
				]
			});
			$('#newEvent_start').clockpicker({
				autoclose: 'true'
			});
			{% if endTime is defined %}
			$('#newEvent_end').clockpicker({
				autoclose: 'true'
			});
			{% endif %}
		});
        $('#newEvent_public').click(function() {
            var inpt = document.getElementById('newEvent_shortDescription');
            if (this.checked) {
                $('#shortDescription').removeClass('hide-for-small-up');
                inpt.required = true;
            } else {
                inpt.required = false;
                var el = document.getElementById('shortDescription');
                if (!el.classList.contains("hide-for-small-up")) {
                    $('#shortDescription').addClass('hide-for-small-up');
                }
            }
        });
	</script>
{% endblock %}