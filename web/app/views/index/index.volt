{% extends "layouts/base.volt" %}
{% block title %}Úvod{% endblock %}
{% block content %}
	<section class="row">
		{% if systemMessage %}
			<div class="large-7 columns">
				<div class="panel">
					<h2><small class="gray">{{ systemMessage.sysm_title }}</small></h2>
						{{ systemMessage.sysm_text }}
					<footer class="text-right">
						<small>{{ systemMessage.sysm_updated }}</small>
					</footer>
				</div>
			</div>
		{% endif %}
		<div class="large-5 columns">
			<header>
				<h2>Prihlásenie</h2>
				<h3 class="subheader gray">Pre vstup do systému sa prosím prihláste!</h3>
			</header>
			<div class="text-center">
				{{ link_to("auth", "Prihlásiť sa", "class" : "button success") }}
			</div>
		</div>
	</section>
{% endblock %}
{% block piwik %}
{% endblock %}