{% extends "layouts/base.volt" %}
{% block title %}{{ announcement.ann_title }}{% endblock %}
{% block content %}
    <div class="row">
        <div class="large-12 columns">
            <article>
                <header>
                    <h2>{{ announcement.ann_title }}</h2>
                    <ul class="inline-list">
                        <li><a href="https://plus.google.com/u/1/{{ announcement.id_user }}/about" target="_blank" class="fi-torso"> {{ announcement.getUser().getFullName() }}</a></li>
                        <li><span class="fi-calendar"> {{ date("j. n. Y", strtotime(announcement.ann_created)) }}</span></li>
                        <li><span class="fi-clock"> {{ date("H:i", strtotime(announcement.ann_created)) }}</span></li>
                        {% if auth.isAdmin() %}
                            <li> | </li>
                            <li><a class="fi-pencil" href="{{ url("announcements/edit/") }}{{ announcement.id_announcement }}"> Upraviť</a> </li>
                            <li><a class="fi-trash" href="{{ url("announcements/delete/") }}{{ announcement.id_announcement }}"> Odstrániť</a> </li>
                            <li><a class="fi-arrow-up"
                                   href="{{ url("announcements/moveUp/") }}{{ announcement.id_announcement }}">
                                    Topovať</a></li>
                        {% endif %}
                        {% if not auth.isAdmin() and announcement.ann_public and auth.isLoggedIn() %}
                            <li> | </li>
                        {% endif %}
                        {% if announcement.ann_public and auth.isLoggedIn() %}
                            <li>{% include "layouts/partials/social-share" with ['announcement': announcement] %}</li>
                        {% endif %}
                    </ul>
                </header>
                {{ announcement.ann_text }}
            </article>
        </div>
    </div>
{% endblock %}