{% extends "layouts/base.volt" %}
{% block title %}Nastavenia{% endblock %}
{% block content %}
	<div class="row">
		<div class="large-12 columns">
			<header><h3>Nastavenia</h3></header>
			<table>
				<thead>
				<tr>
					<th>Pri odhlasovaní odhlásiť aj z Googlu</th>
					<th>Akcie</th>
				</tr>
				</thead>
				<tbody>
				{% for record in setting %}
					<tr>
						<td><span class="label {{ record.set_logoutGoogle ? 'success' : 'alert' }}">{{ record.set_logoutGoogle ? 'Áno' : 'Nie' }}</span></td>
						<td>
							<a href="{{ url("settings/editSetting/") }}{{ record.id_setting }}" class="fi-pencil"> Upraviť</a>
						</td>
					</tr>
				{% else %}
					<tr><td colspan="5" class="text-center"><span>Žiaden záznam.</span></td></tr>
				{% endfor %}
				</tbody>
			</table>
		</div>
		<div class="large-12 columns">
			<header><h3>Zasielanie zmien na e-mail</h3></header>
			<table>
				<thead>
				<tr>
					<th>E-mail</th>
					<th>Oznamy</th>
					<th>Udalosti</th>
					<th>Suplovanie</th>
					<th>Akcie</th>
				</tr>
				</thead>
				<tbody>
				{% for record in newsletter %}
					<tr>
						<td>{{ record.nws_email }}</td>
						<td><span class="label {{ record.nws_announcements ? 'success' : 'alert' }}">{{ record.nws_announcements ? 'Aktívne' : 'Neaktívne' }}</span></td>
						<td><span class="label {{ record.nws_events ? 'success' : 'alert' }}">{{ record.nws_events ? 'Aktívne' : 'Neaktívne' }}</span></td>
						<td>
							{% if record.nws_suplo == 'ALL' %}
								<span class="label success">Celé</span>
							{% elseif record.nws_suplo == 'ME' %}
								<span class="label success">Moje</span>
							{% else %}
								<span class="label alert">Žiadne</span>
							{% endif %}

						</td>
						<td>
							<a href="{{ url("settings/editNewsletter/") }}{{ record.id_newsletter }}" class="fi-pencil"> Upraviť</a>
							|
							<a href="{{ url("settings/deleteNewsletter/") }}{{ record.id_newsletter }}" class="fi-trash"> Odstrániť</a>
						</td>
					</tr>
				{% else %}
					<tr><td colspan="5" class="text-center"><span>Zatiaľ nemáte zaregistrovanú žiadnu e-mailovú adresu.</span></td></tr>
				{% endfor %}
				</tbody>
				<tfoot>
					<tr>
						<td style="text-align: right; border-top: 1px solid gray" colspan="6">
							{{ link_to("settings/newNewsletter", "Pridať e-mail", "class" : "tiny button", "style" : "margin: 5px 0") }}
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
{% endblock %}