{% extends "layouts/base.volt" %}
{% block title %}Upraviť e-mail{% endblock %}
{% block content %}
	<div class="row">
		<div class="large-12 columns">
			<header class="row">
				<div class="large-12 columns">
					<h2>Upraviť e-mail</h2>
				</div>
			</header>
			<div class="row">
				<div class="large-12 columns">
					<form id="formEditNewsletter" name="formEditNewsletter" action="{{ url("settings/editNewsletter/") }}{{ newsletter.id_newsletter }}" enctype="application/x-www-form-urlencoded" method="post">
						<div class="row">
							<div class="small-6 columns end">
								<label>E-mail
									<input type="email" id="newNewsletter_email" name="newNewsletter_email" placeholder="E-mail" disabled value="{{ newsletter.nws_email }}"/>
								</label>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<table>
									<tr>
										<th class="text-right">Oznamy:</th>
										<td class="text-center">
											<input type="checkbox" value="1" id="editNewsletter_announcements" name="editNewsletter_announcements" {{ newsletter.nws_announcements ? 'checked' : ''}} />
											<label for="editNewsletter_announcements">Zasielať oznamy</label>
										</td>
									</tr>
									<tr>
										<th class="text-right">Udalosti:</th>
										<td class="text-center">
											<input type="checkbox" value="1" id="editNewsletter_events" name="editNewsletter_events" {{ newsletter.nws_events ? 'checked' : ''}}/>
											<label for="editNewsletter_events">Zasielať udalosti</label>
										</td>
									</tr>
									<tr>
										<th class="text-right">Suplovanie:</th>
										<td class="text-center">
											<input type="radio" id="editNewsletter_suploAll" name="editNewsletter_suplo" value="all" {{ newsletter.nws_suplo == 'ALL' ? 'checked' : ''}}/><label for="editNewsletter_suploAll">Celé suplovanie</label>
											<input type="radio" id="editNewsletter_suploMe" name="editNewsletter_suplo" value="me" {{ newsletter.nws_suplo == 'ME' ? 'checked' : ''}}/><label for="editNewsletter_suploMe">Moje suplovanie</label>
											<input type="radio" id="editNewsletter_suploNone" name="editNewsletter_suplo" value="none" {{ newsletter.nws_suplo == 'NONE' ? 'checked' : ''}}/><label for="editNewsletter_suploNone">Nezasielať suplovanie</label>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="large-5 large-centered columns">
								<button type="submit" style="margin-top: 20px;" class="button expand">Odoslať</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
{% endblock %}