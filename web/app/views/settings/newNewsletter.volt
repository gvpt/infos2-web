{% extends "layouts/base.volt" %}
{% block title %}Pridať e-mail{% endblock %}
{% block content %}
	<div class="row">
		<div class="large-12 columns">
			<header class="row">
				<div class="large-12 columns">
					<h2>Pridať e-mail</h2>
				</div>
			</header>
			<div class="row">
				<div class="large-12 columns">
					<form id="formNewNewsletter" name="formNewNewsletter" action="{{ url("settings/newNewsletter") }}" enctype="application/x-www-form-urlencoded" method="post" data-abide>
						<div class="row">
							<div class="small-6 columns end">
								<label>E-mail
									<input type="email" id="newNewsletter_email" name="newNewsletter_email" placeholder="E-mail" required />
								</label>
								<small class="error">E-mail</small>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<table>
									<tr>
										<th class="text-right">Oznamy:</th>
										<td class="text-center">
											<input type="checkbox" value="1" id="newNewsletter_announcements" name="newNewsletter_announcements"/>
											<label for="newNewsletter_announcements">Zasielať oznamy</label>
										</td>
									</tr>
									<tr>
										<th class="text-right">Udalosti:</th>
										<td class="text-center">
											<input type="checkbox" value="1" id="newNewsletter_events" name="newNewsletter_events"/>
											<label for="newNewsletter_events">Zasielať udalosti</label>
										</td>
									</tr>
									<tr>
										<th class="text-right">Suplovanie:</th>
										<td class="text-center">
											<input type="radio" id="newNewsletter_suploAll" name="newNewsletter_suplo" value="all"/><label for="newNewsletter_suploAll">Celé suplovanie</label>
											<input type="radio" id="newNewsletter_suploMe" name="newNewsletter_suplo" value="me"/><label for="newNewsletter_suploMe">Moje suplovanie</label>
											<input type="radio" id="newNewsletter_suploNone" name="newNewsletter_suplo" value="none" checked/><label for="newNewsletter_suploNone">Nezasielať suplovanie</label>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="large-5 large-centered columns">
								<button type="submit" style="margin-top: 20px;" class="button expand">Odoslať</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
{% endblock %}