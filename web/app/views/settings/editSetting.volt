{% extends "layouts/base.volt" %}
{% block title %}Upraviť nastavenia{% endblock %}
{% block content %}
    <div class="row">
        <div class="large-12 columns">
            <header class="row">
                <div class="large-12 columns">
                    <h2>Upraviť nastavenia</h2>
                </div>
            </header>
            <div class="row">
                <div class="large-12 columns">
                    <form id="formEditSetting" name="formEditSetting" action="{{ url("settings/editSetting/") }}{{ setting.id_setting }}" enctype="application/x-www-form-urlencoded" method="post">
                        <div class="row">
                            <div class="large-12 columns">
                                <table>
                                    <tr>
                                        <th class="text-right">Pri odhlasovaní odhlásiť aj z Googlu:</th>
                                        <td class="text-center">
                                            <input type="checkbox" value="1" id="editSetting_logoutGoogle" name="editSetting_logoutGoogle" {{ setting.set_logoutGoogle ? 'checked' : ''}} />
                                            <label for="editSetting_announcements">Áno</label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-5 large-centered columns">
                                <p>Upozornenie zmena sa prejaví až po odhlásení a opätovnom prihlásení.</p>
                                <button type="submit" style="margin-top: 20px;" class="button expand">Odoslať</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
{% endblock %}