{% for record in suplo %}
	<tr data-ajax="{{ url("suplo/record/") }}{{ record.id_suplo }}">
		<td>{{ date("j. n. Y", strtotime(record.sup_date)) }}</td>
		<td>{{ record.getHour().tmt_label }}</td>
		<td>{{ record.sup_subject }}</td>
		<td>{{ record.getMissing().getFullName() }}</td>
	</tr>
{% else %}
	<tr><th class="text-center" colspan="4">Tento mesiac ste ešte nesuplovali!</th></tr>
{% endfor %}