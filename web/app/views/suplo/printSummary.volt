{% extends "layouts/print.volt" %}
{% block title %}Výkaz nadčasov a suplovania{% endblock %}
{% block content %}
	<h1>Výkaz nadčasov a suplovania</h1>
	<p style="text-align: center">
		<span class="fi-torso" style="margin-right: 8px;"> {{ auth.getName() }} </span>
		<span class="fi-clock">  {{ season.format("F Y") }}</span>
	</p>
	<table>
		<thead>
			<tr>
				<td>Dátum</td>
				<td>Trieda</td>
				<td>Hodina</td>
				<td>Predmet</td>
				<td>Namiesto</td>
			</tr>
		</thead>
		<tbody id="suploHistory">
		{% for record in suplo %}
			<tr data-ajax="{{ url("suplo/record/") }}{{ record.id_suplo }}">
				<td>{{ date("j. n. Y", strtotime(record.sup_date)) }}</td>
				<td>{{ record.sup_classes }}</td>
				<td>{{ record.getHour().tmt_label }}</td>
				<td>{{ record.sup_subject }}</td>
				<td>{{ record.getMissing().getFullName() }}</td>
			</tr>
		{% else %}
			<tr><th class="text-center">Tento mesiac ste ešte nesuplovali!</th></tr>
		{% endfor %}
		</tbody>
		<tfoot>

		</tfoot>
	</table>
	<table style="margin-top: 2.5cm;">
		<tr>
			<td colspan="3" style="text-align: right">Suplované a nadčasové hodiny chcem:</td>
			<td style="text-align: right">preplatiť</td>
			<td style="text-align: center"> - </td>
			<td style="text-align: left">dať na NV</td>
		</tr>
		<tfoot>
			<tr >
				<td colspan="3" style="text-align: right;" >Podpis:</td>
				<td colspan="3"></td>
			</tr>
		</tfoot>

	</table>
{% endblock %}