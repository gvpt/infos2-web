{% extends "layouts/base.volt" %}
{% block title %}Nové suplovanie{% endblock %}
{% block content %}
	<div class="row">
		<div class="large-12 columns">
			<header class="row">
				<div class="large-12 columns">
					<h2>Suplovanie</h2>
				</div>
			</header>
			<div class="row">
				<div class="large-12 columns">
					<form id="formNewSuplo" name="formNewSuplo" action="{{ url("suplo/new") }}" enctype="application/x-www-form-urlencoded" method="post" data-abide>
						<div class="row">
							<div class="small-6 columns">
								<label>
									<input type="text" id="newSuplo_date" name="newSuplo_date" placeholder="Dátum" required value="{{ date }}" pattern="(0[1-9]|[12][0-9]|3[01])[- \.](0[1-9]|1[012])[- \.](19|20)\d\d" data-check-url="{{ config.application.baseUri }}suplo/exists/" />
								</label>
								<small class="error">Dátum je povinný.</small>
							</div>
							<div class="small-6 columns end">
								<div class="hide">
									<div class="alert-box warning" id="suploExists">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<label for="newSuplo_text">Suplovanie</label>
								<textarea id="newSuplo_text" name="newSuplo_text" style="height: 400px; width: 100%;"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="large-5 large-centered columns">
								<button type="submit" style="margin-top: 20px;" class="button expand">Odoslať</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
{% endblock %}
{% block javascript %}
	<script>
		$("#newSuplo_date").datepicker({
			dateFormat: "dd.mm.yy",
			firstDay: 1,
			dayNamesMin:["Ne", "Po", "Ut", "St", "Št", "Pia", "So"],
			monthNames: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
			beforeShowDay: $.datepicker.noWeekends
		});
	</script>
{% endblock %}