<article>
	<h1 class="fi-clock"> {{ record.getHour().tmt_label }}</h1>
	<ul class="inline-list">
		<li><span class="fi-torso"> {{ record.getMissing().getFullName() }}</span></li>
		<li><span class="fi-torsos-all"> {{ record.getHour().tmt_label }}</span></li>
		<li><span class="fi-calendar"> {{ date("j. n. Y", strtotime(record.sup_date)) }}</span></li>
		<li><span class="fi-map"> {{ record.sup_classroom }}</span></li>
		<li><span class="fi-projection-screen"> {{ record.sup_subject }}</span> </li>
		{% if record.sup_note %}
			<li><span class="fi-info"> {{ record.sup_note }}</span> </li>
		{% endif %}
	</ul>
</article>
<a class="close-reveal-modal">&#215;</a>
