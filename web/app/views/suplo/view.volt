{% extends "layouts/base.volt" %}
{% block title %}Suplovanie {{ date.format("j. n. Y") }}{% endblock %}
{% block content %}
	<div class="row">
		<div class="large-12 columns">
			<header class="row">
				<div class="large-12 columns">
					<h2>Suplovanie</h2>
				</div>
			</header>
			<div class="row">
				<div class="small-1 columns">
					<label for="suploFilter_date" class="right inline">Dátum</label>
				</div>
				<div class="small-4 columns">
					<form action="{{ url("suplo/view/") }}" name="formSuploFilter" id="formSuploFilter" method="post">
						<input id="suploFilter_date" name="suploFilter_date" type="text" value="{{ date.format("d.m.Y") }}" required />
					</form>
				</div>
			</div>
			<div class="row">
				<div class="large-12 columns">
					<table style="width: 100%;">
						<thead>
							<tr>
								<th>Hodina</th>
								<th>Vyučujúci</th>
								<th>Trieda</th>
								<th>Predmet</th>
								<th>Učebňa</th>
								<th>Zastupujúci</th>
								<th>Poznámka</th>
							</tr>
						</thead>
						<tbody id="suploContainer">
							{% for record in suplo %}
								<tr data-ajax="{{ url("suplo/record/") }}{{ record.id_suplo }}">
									<td>{{ record.getHour().tmt_label }}</td>
									<td>{{ record.getMissing().usr_firstName }} {{ record.getMissing().usr_lastName }}</td>
									<td>{{ record.sup_classes }}</td>
									<td>{{ record.sup_subject }}</td>
									<td>{{ record.sup_classroom }}</td>
									<td>{{ record.getUser().usr_firstName }} {{ record.getUser().usr_lastName }}</td>
									<td>{{ record.sup_note }}</td>
								</tr>
							{% else %}
								<tr>
									<td colspan="7" class="text-center">
										Suplovanie pre tento dátum ešte nebolo zverejnené.
									</td>
								</tr>
							{% endfor %}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
{% endblock %}
{% block javascript %}
	<script>
		$("#suploFilter_date").datepicker({
			dateFormat: "dd.mm.yy",
			firstDay: 1,
			dayNamesMin:["Ne", "Po", "Ut", "St", "Št", "Pia", "So"],
			monthNames: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
			beforeShowDay: $.datepicker.noWeekends,
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true
		});
	</script>
{% endblock %}