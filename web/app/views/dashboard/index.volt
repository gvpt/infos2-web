{% extends "layouts/base.volt" %}
{% block title %}Dashboard{% endblock %}
{% block navigation %}{% endblock %}
{% block content %}
	<div class="row">
		<div class="large-7 columns">
			<section id="panelAnnouncements">
				<div class="boardPanel large-12 columns">
					<header class="row">
						<div class="large-12 columns">
							{{ link_to("announcements", "<h2>Oznamy</h2>") }}
						</div>
					</header>
					{% for announcement in announcements %}
						<article class="row">
							<div class="large-12 columns">
								<header class="row">
									<div class="small-8 columns"><h3><a href="{{ url("announcements/view/") }}{{ announcement.id_announcement }}">{{ announcement.ann_title }}</a></h3></div>
								</header>
								<div class="row">
									<div class="large-12 columns">
										{{ announcement.ann_text }}
									</div>
								</div>
								<hr />
								<footer class="row">
									<div class="large-12 columns">
										<ul class="inline-list" style="font-size: small;">
											<li><a href="https://plus.google.com/u/1/{{ announcement.id_user }}/about" target="_blank" class="fi-torso"> {{ announcement.getUser().getFullName() }}</a></li>
											<li><span class="fi-calendar"> {{ date("j. n. Y", strtotime(announcement.ann_created)) }}</span></li>
											<li><span class="fi-clock"> {{ date("H:i", strtotime(announcement.ann_created)) }}</span></li>
											{% if auth.isAdmin() %}
												<li> | </li>
												<li><a class="fi-pencil" href="{{ url("announcements/edit/") }}{{ announcement.id_announcement }}"> Upraviť</a> </li>
												<li><a class="fi-trash" href="{{ url("announcements/delete/") }}{{ announcement.id_announcement }}"> Odstrániť</a> </li>
												<li><a class="fi-arrow-up"
													   href="{{ url("announcements/moveUp/") }}{{ announcement.id_announcement }}">
														Topovať</a></li>
											{% endif %}
											{% if not auth.isAdmin() and announcement.ann_public %}
												<li> | </li>
											{% endif %}
											{% if announcement.ann_public %}
												<li>{% include "layouts/partials/social-share" with ['announcement': announcement] %}</li>
											{% endif %}
										</ul>
									</div>
								</footer>
							</div>
						</article>
					{% else %}
						<article class="row">
							<div class="large-12 columns">
								Nie su zverejnené žiadne oznamy.
							</div>
						</article>
					{% endfor %}
				</div>
			</section>
		</div>

		<div class="large-5 columns">
			<section class="userPanel" id="panelUser">
				<header><h2>{{ session.get('auth-identity')['name'] }}</h2></header>
				<time datetime="{{ date("c") }}" id="serverTime">{{ date("d.m.Y H:i") }}</time>
				<table id="aktualne">
					<tr>
						<td>Aktuálne:</td>
						<td id="current">{{ current.tmt_label }}</td>
					</tr>
					<tr>
						<td>Nasleduje:</td>
						<td id="next">{{ next.tmt_label }}</td>
					</tr>
				</table>
				<hr />
				{% if auth.isRoot() %}
					<span>
						<a href="{{ url("system") }}">{{ image("images/dashboard/settings.png", "alt": "Systemove nastavenia", "title" : "Systemove nastavenia", "class" : "icon") }}</a>
					</span>
					<span class="right">
						<a href="https://gymmt.edupage.org/login/index.php" target="_blank">{{ image("images/dashboard/izk.png", "alt": "Žiacka knižka", "title" : "Žiacka knižka", "class" : "icon") }}</a>
						<a href="{{ url("settings") }}">{{ image("images/dashboard/settings.png", "alt": "Nastavenia", "title" : "Nastavenia", "class" : "icon") }}</a>
						{% if auth.logoutGoogle() %}
							<a href="{{ url("auth/logoutGoogle") }}">{{ image("images/dashboard/logout.png", "alt": "Odhlásiť sa", "title" : "Odhlásiť sa", "class" : "icon") }}</a>
						{% else %}
							<a href="{{ url("auth/logout") }}">{{ image("images/dashboard/logout.png", "alt": "Odhlásiť sa", "title" : "Odhlásiť sa", "class" : "icon") }}</a>
						{% endif %}
					</span>
				{% else %}
					<div class="text-right">
						<a href="https://gymmt.edupage.org/login/index.php" target="_blank">{{ image("images/dashboard/izk.png", "alt": "Žiacka knižka", "title" : "Žiacka knižka", "class" : "icon") }}</a>
						<a href="{{ url("settings") }}">{{ image("images/dashboard/settings.png", "alt": "Nastavenia", "title" : "Nastavenia", "class" : "icon") }}</a>
						{% if auth.logoutGoogle() %}
							<a href="{{ url("auth/logoutGoogle") }}">{{ image("images/dashboard/logout.png", "alt": "Odhlásiť sa", "title" : "Odhlásiť sa", "class" : "icon") }}</a>
						{% else %}
							<a href="{{ url("auth/logout") }}">{{ image("images/dashboard/logout.png", "alt": "Odhlásiť sa", "title" : "Odhlásiť sa", "class" : "icon") }}</a>
						{% endif %}
					</div>
				{% endif %}
			</section>

			{% if auth.isAdmin() %}
				<section class="text-center">
					<ul class="small-block-grid-3">
						<li>{{ link_to("announcements/new", "Pridať oznam", "class" : "button small") }}</li>
						<li>
							<a href="#" data-dropdown="eventsDropdown" class="button small dropdown">Pridať udalosť</a>
							<ul id="eventsDropdown" data-dropdown-content class="f-dropdown">
								{% for eventType in eventTypes %}
									<li><a href="{{ url("events/new/") }}{{ eventType.evy_type }}">{{ eventType.evy_label }}</a></li>
								{% endfor %}
							</ul>
						</li>
						<li>{{ link_to("suplo/new", "Pridať suplovanie", "class" : "button small") }}</li>
					</ul>
				</section>
			{% endif %}

			<section class="boardPanel" id="panelSuplo">
				<header>
					<div class="large-12 columns">
						<a target="_blank" href="http://gymmt.edupage.org/substitution/view.php"><h2>Suplovanie</h2></a>

                    </div>
				</header>
				<table style="width: 100%">
					<thead>
					<tr>
						<td>Hodina</td>
						<td>Trieda</td>
						<td>Predmet</td>
						<td>Učebňa</td>
						<td>Namiesto</td>
					</tr>
					</thead>
					<tbody>
					{% if suploUserToday.count() > 0 %}
						<tr><th colspan="5" class="text-center">Dnes</th></tr>
						{% for record in suploUserToday %}
							<tr data-ajax="{{ url("suplo/record/") }}{{ record.id_suplo }}">
								<td>{{ record.getHour().tmt_label }}</td>
								<td>{{ record.sup_classes }}</td>
								<td>{{ record.sup_subject }}</td>
								<td>{{ record.sup_classroom }}</td>
								<td>{{ record.getMissing().getFullName() }}</td>
							</tr>
						{% endfor %}
					{% else %}
						<tr><th colspan="5" class="text-center"> <a target="_blank" href="http://gymmt.edupage.org/substitution/view.php">Suplovanie TU.</a></th></tr>
						<!--<tr><th colspan="5" class="text-center">Na dnes nemáte žiadne suplovanie!</th></tr>-->
					{% endif %}
					{% if suploUserTomorrow.count() > 0 %}
						<tr><th colspan="5" class="text-center">Zajtra</th></tr>
						{% for record in suploUserTomorrow %}
							<tr data-ajax="suplo/record/{{ record.id_suplo }}">
								<td>{{ record.getHour().tmt_label }}</td>
								<td>{{ record.sup_classes }}</td>
								<td>{{ record.sup_subject }}</td>
								<td>{{ record.sup_classroom }}</td>
								<td>{{ record.getMissing().getFullName() }}</td>
							</tr>
						{% endfor %}
					{% else %}
						<!--<tr><th colspan="5" class="text-center">Na zajtra nemáte žiadne suplovanie!</th></tr>-->
					{% endif %}
					</tbody>
				</table>
			</section>

			<section class="boardPanel" id="panelEvents">
				<header>
					<div class="large-12 columns">
						{{ link_to("events", "<h2>Udalosti</h2>") }}
					</div>
				</header>
				<table style="width: 100%">
					<thead>
					<tr>
						<td>Typ</td>
						<td>Názov</td>
						<td>Čas</td>
						<td>Miesto</td>
					</tr>
					</thead>
					<tbody>
					{% for event in events %}
						<tr data-url="{{ url("events/view/") }}{{ event.id_event }}">
							<td class="text-center"><img src="{{ url("images/events/") }}{{ event.getEventType().evy_type }}_32.png" alt="{{ event.getEventType().evy_label }}" title="{{ event.getEventType().evy_label }}"></td>
							<td>{{ event.evt_title }}</td>
							<td>{{ date("j. n. Y", strtotime(event.evt_date)) }}</td>
							<td>{{ event.evt_place }}</td>
						</tr>
					{% endfor %}
					</tbody>
				</table>
			</section>

			<section class="boardPanel" id="panelDocs">
				<header>
					<div class="small-12 columns">
						<h2>Dokumenty</h2>
					</div>
				</header>
				<table style="width: 100%">
					<tbody>
					<tr data-url="https://drive.google.com/a/gymmt.sk/folderview?id=0B30M7T6N6wkbTWZlY2FBTHJhN2M&usp=sharing">
						<td><span class="fi-folder" style="font-size: large"> Pracovné normy</span> </td>
					</tr>
					<tr data-url="https://drive.google.com/a/gymmt.sk/folderview?id=0B30M7T6N6wkbSmJrczZPX1JYRG8&usp=sharing">
						<td><span class="fi-folder" style="font-size: large"> Formuláre</span> </td>
					</tr>
					</tbody>
				</table>
			</section>

			<section class="boardPanel" id="panelSuploHistory">
				<header>
					<div class="small-10 columns">
						<h2>História suplovania</h2>
					</div>
					<div class="small-2 columns text-right">
						<a href="{{ url("suplo/summary/") }}{{ date('m-Y') }}" target="_blank" id="printSummary">{{ image("images/dashboard/print.png", "alt": "Tlačiť", "title" : "Tlačiť", "style" : "margin-top: 12px") }}</a>
					</div>
				</header>
				<form id="formSuploHistory" name="formSuploHistory" action="{{ url("suplo/summary/") }}" data-action-url="{{ url("suplo/summary/") }}">
					<select name="suploHistory_season" id="suploHistory_season">
						<option value="{{ date('m-Y') }}" selected>Aktuálne obdobie</option>
						<option disabled>---</option>
						{% for season in suploSeasons %}
							<option value="{{ season.season_value }}">{{ season.season_title }}</option>
						{% endfor %}
					</select>
				</form>
				<table style="width: 100%">
					<thead>
					<tr>
						<td>Dátum</td>
						<td>Hodina</td>
						<td>Predmet</td>
						<td>Namiesto</td>
					</tr>
					</thead>
					<tbody id="suploHistory">
					{% for record in suploHistory %}
						<tr data-ajax="{{ url("suplo/record/") }}{{ record.id_suplo }}">
							<td>{{ date("j. n. Y", strtotime(record.sup_date)) }}</td>
							<td>{{ record.getHour().tmt_label }}</td>
							<td>{{ record.sup_subject }}</td>
							<td>{{ record.getMissing().getFullName() }}</td>
						</tr>
					{% else %}
						<tr><th class="text-center" colspan="4">Tento mesiac ste ešte nesuplovali!</th></tr>
					{% endfor %}
					</tbody>
				</table>
			</section>
		</div>
	</div>
{% endblock %}