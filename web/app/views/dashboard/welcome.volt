{% extends "layouts/base.volt" %}
{% block title %}Prvé spustenie{% endblock %}
{% block navigation %}{% endblock %}
{% block content %}
	<section class="row">
		<div class="large-12 columns">
			<h2>Prvé spustenie</h2>
			<div class="panel">
				<h2><small class="gray">Systémová správa</small></h2>
				<p class="text-justify">
				</p>
				<footer class="text-right">
					<small>28.03.2014 o 17:05</small>
				</footer>
			</div>
			<a href="{{ auth.createLoginUrl('offline') }}" class="button success expand">Pokračovať</a>
		</div>
	</section>
{% endblock %}