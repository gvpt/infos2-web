<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>{% block title %}{% endblock %} - Infos2</title>
	<meta name="robots" content="noindex, nofollow" />
	<meta name="author" content="Jakub Dubec" />
	<meta name="viewport" content="width=device-width" />

	<!-- CSS -->
	{{ stylesheet_link("css/normalize.css") }}
	{{ stylesheet_link("css/print.css") }}

	<!-- Favicon -->
	<link rel="shortcut icon" href="{{ url("images/favicon.ico") }}" type="image/x-icon">
	<link rel="icon" href="{{ url("images/favicon.ico") }}" type="image/x-icon">

</head>
<body>

<!-- block content -->
{% block content %}
{% endblock %}

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(['setCustomVariable',
		1,
		"User",
		"{{ session.get('auth-identity')['name'] }}",
		"visit"
	]);
	_paq.push(['setCustomVariable',
		2,
		"Role",
		"{{ session.get('auth-identity')['role'] }}",
		"visit"
	]);
	_paq.push(['trackGoal', 3]);
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(['setTrackerUrl', u+'piwik.php']);
		_paq.push(['setSiteId', 1]);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
		g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	})();
</script>

<script>
	window.print();
</script>

</body>
</html>