<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>{% block title %}{% endblock %} - Infos2</title>
	<meta name="robots" content="noindex, nofollow" />
	<meta name="author" content="Jakub Dubec" />
	<meta name="viewport" content="width=device-width" />
	<meta name="google-signin-client_id" content="180622801359.apps.googleusercontent.com">
	{% if refresh is defined %}
	<meta http-equiv="refresh" content="180" />
	{% endif %}

	<!-- CSS -->
	{{ stylesheet_link("css/normalize.css") }}
	{{ stylesheet_link("css/app.css") }}
	{% if jqueryui is defined %}
		{{ stylesheet_link("css/jquery-ui/jquery-ui.min.css") }}
		{{ stylesheet_link("css/jquery-ui/jquery-ui.structure.min.css") }}
		{{ stylesheet_link("css/jquery-ui/jquery-ui.theme.min.css") }}
	{% endif %}

	<!-- Favicon -->
	<link rel="shortcut icon" href="{{ url("images/favicon.ico") }}" type="image/x-icon">
	<link rel="icon" href="{{ url("images/favicon.ico") }}" type="image/x-icon">

	<!-- JS -->
	{{ javascript_include("js/vendor/modernizr.min.js") }}
	{{ javascript_include("js/jquery/jquery.min.js") }}

	<!-- Bolo to pouzivane?
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	-->

	<!-- Jebnuty explorer! -->
	<!--[if lt IE 8]><script type="text/javascript">alert("Your browser is obsolete, please use Mozilla Firefox!");</script><![endif]-->
	<!--[if IE]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</head>
<body>

<!-- block navigation -->
{% block navigation %}
<div class="contain-to-grid">
	<nav class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<a href="{{ url("") }}"><h1><img src="{{ url("images/logo.png") }}" alt="Infos2" style="height: 30px;"><span class="hide">Infos2</span></h1></a>
			</li>
			<li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
		</ul>
		<section class="top-bar-section">
			<ul class="right">
				{% if auth.isLoggedIn() %}
					{% set menus = [
						'Oznamy': 'announcements',
						'Suplovanie': 'suplo',
						'Udalosti': 'events'
					] %}
					{% for key, value in menus %}
						{% if value == dispatcher.getControllerName() %}
							<li class="active">{{ link_to(value, key) }}</li>
						{% else %}
							<li>{{ link_to(value, key) }}</li>
						{% endif %}
					{% endfor %}
					<li class="has-dropdown">
						<a href="#">Dokumenty</a>
						<ul class="dropdown">
							<li><a href="https://drive.google.com/a/gymmt.sk/folderview?id=0B30M7T6N6wkbTWZlY2FBTHJhN2M&usp=sharing" target="_blank">Pracovné normy</a></li>
							<li><a target="_blank" href="https://drive.google.com/a/gymmt.sk/folderview?id=0B30M7T6N6wkbSmJrczZPX1JYRG8&usp=sharing">Formuláre</a></li>
						</ul>
					</li>
					<li class="divider"></li>
					<li class="has-dropdown">
						<a href="#">{{ session.get('auth-identity')['name'] }}</a>
						<ul class="dropdown">
							<li>{{ link_to("settings", "Nastavenia") }}</li>
							<li class="divider"></li>
							{% if auth.logoutGoogle() %}
								<li>{{ link_to("auth/logoutGoogle", "Odhlásiť sa") }}</li>
							{% else %}
								<li>{{ link_to("auth/logout", "Odhlásiť sa") }}</li>
							{% endif %}
						</ul>
					</li>
				{% else %}
					<li class="has-form">
						{{ link_to("auth", "Prihlásiť sa", "class" : "button success") }}
					</li>
				{% endif %}
			</ul>
		</section>
	</nav>
</div>
{% endblock %}

{% if systemNotification %}
	<div class="row" style="margin-top: 20px;">
		<div class="large-12 columns">
			<div data-alert class="alert-box warning">
				{{ systemNotification.sysm_text }}<a href="#" class="close">&times;</a>
			</div>
		</div>
	</div>
{% endif %}

{% set flashMessages = flash.getMessages() %}
{% if flashMessages|length > 0 %}
	<div class="row" style="margin-top: 20px;">
		<div class="large-12 columns">
			{% for type, messages in flashMessages %}
				{% for message in messages %}
					{{ flash.outputMessage(type, message) }}
				{% endfor %}
			{% endfor %}
		</div>
	</div>
{% endif %}
<!-- block content -->
{% block content %}
{% endblock %}


<!-- block footer -->
{% block footer %}
	<footer id="footer">
		<div class="row">
			<div class="large-3 columns">
				{{ image("images/logo.png", "alt": "Infos", "title" : "Infos", "style" : "height:30px; width: auto; margin:5px;") }}
				<p class="text-right" style="color: #fff;"><small style="font-size: x-small;">by <a href="http://jakubdubec.me" target="_blank" rel="author">Jakub Dubec</a>, Juraj Michalek &copy; 2014 - {{ date('Y') }}</small></p>
			</div>
			<div class="large-5 columns end">
				<ul class="inline-list text-left">
					<li><a href="http://infos2.jakubdubec.me">O projekte</a></li>
					<li><a target="_blank" href="{{ config.application.baseUri }}about/blog">Blog</a></li>
					<li><a href="{{ config.application.baseUri }}about/bug">Nahlasit chybu</a></li>
				</ul>
			</div>
		</div>
	</footer>
{% endblock %}

{% block reveal %}
	<div id="myModal" class="reveal-modal small" data-reveal>
	</div>
{% endblock %}

<!-- JS -->
{{ javascript_include("js/jquery/jquery.cookie.min.js") }}
{{ javascript_include("js/jquery/jquery.placeholder.min.js") }}
{{ javascript_include("js/jquery/jquery.min.js") }}
{{ javascript_include("js/vendor/fastclick.min.js") }}
{{ javascript_include("js/foundation.min.js") }}
{{ javascript_include("js/app.min.js") }}

{% if tinymce is defined %}
	{{ javascript_include("js/tinymce/tinymce.min.js") }}
{% endif %}
{% if jqueryui is defined %}
	{{ javascript_include("js/jquery-ui/jquery-ui.min.js") }}
{% endif %}
{% if timepicker is defined %}
	{{ javascript_include("js/jquery/jquery-clockpicker.min.js") }}
{% endif %}
{% if momentjs is defined %}
	{{ javascript_include("js/vendor/moment.min.js") }}
{% endif %}
{% if eventjs is defined %}
	{{ javascript_include("js/event.js") }}
{% endif %}
{% block javascript %}
{% endblock %}
{% block piwik %}
		<!-- Piwik -->
		<script type="text/javascript">
			var _paq = _paq || [];
			_paq.push(["setDomains", ["*.infos.gvpt.sk"]]);
			// you can set up to 5 custom variables for each visitor
			{% if auth.isLoggedIn() %}
			_paq.push(["setCustomVariable", 1, "User", "{{ auth.getName() }}", "visit"]);
			_paq.push(["setCustomVariable", 2, "Role", "{{ session.get('auth-identity')['role'] }}", "visit"]);
			{% endif %}
			_paq.push(['trackPageView']);
			_paq.push(['enableLinkTracking']);
			(function() {
				var u="//piwik.gvpt.sk/";
				_paq.push(['setTrackerUrl', u+'piwik.php']);
				_paq.push(['setSiteId', 3]);
				var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
				g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
			})();
		</script>
		<noscript><p><img src="//piwik.gvpt.sk/piwik.php?idsite=3" style="border:0;" alt="" /></p></noscript>
		<!-- End Piwik Code -->
{% endblock %}
</body>
</html>