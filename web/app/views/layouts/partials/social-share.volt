<a data-dropdown="social-share" aria-controls="social-share" aria-expanded="false"><span class="fi-share"> Zdielať</span></a>
<ul id="social-share" class="f-dropdown inline-list" data-dropdown-content aria-hidden="true" tabindex="-1">
    <li class="social-facebook">
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ url("index/view/") }}{{ announcement.ann_uid }}&t={{ announcement.ann_title }}" onclick="return !window.open(this.href, 'Share on Facebook', 'width=500,height=500')" target="_blank">
            <i class="fi-social-facebook"></i>
        </a>
    </li>
    <li class="social-googleplus">
        <a href="https://plus.google.com/share?url=Infos - oznam: {{ announcement.ann_title }} odkaz - {{ url("index/view/") }}{{ announcement.ann_uid }}" onclick="return !window.open(this.href, 'Share on Google+', 'width=500,height=500')" target="_blank">
            <i class="fi-social-google-plus"></i>
        </a>
    </li>
    <li class="social-twitter">
        <a href="https://twitter.com/intent/tweet?text=Infos - oznam: {{ announcement.ann_title }} odkaz - {{ url("index/view/") }}{{ announcement.ann_uid }}" onclick="return !window.open(this.href, 'Share on Twitter', 'width=500,height=500')" target="_blank">
            <i class="fi-social-twitter"></i>
        </a>
    </li>
    <li class="social-email">
        <a href="mailto:?subject=Infos - oznam: {{ announcement.ann_title }}&amp;body=Odkaz - {{ url("index/view/") }}{{ announcement.ann_uid }}">
            <i class="fi-mail"></i>
        </a>
    </li>
    <li class="social-print">
        <a href="javascript:window.print()">
            <i class="fi-print"></i>
        </a>
    </li>
</ul>