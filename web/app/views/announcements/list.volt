{% extends "layouts/base.volt" %}
{% block title %}Oznamy - {{ offset }}{% endblock %}
{% block content %}
	<div class="row">
		<div class="large-12 columns">
			{% for announcement in pagination.items %}
				<article class="row">
					<div class="large-12 columns">
						<header class="row">
							<div class="large-12 columns"><h2>
									<a href="{{ url("announcements/view/") }}{{ announcement.id_announcement }}">{{ announcement.ann_title }}</a>
								</h2>
							</div>
						</header>
						<div class="row">
							<div class="large-12 columns">
								{{ announcement.ann_text }}
							</div>
						</div>
						<hr />
						<footer class="row">
							<div class="large-12 columns">
								<ul class="inline-list" style="font-size: small;">
									<li><a href="https://plus.google.com/u/1/{{ announcement.id_user }}/about" target="_blank" class="fi-torso"> {{ announcement.getUser().getFullName() }}</a></li>
									<li><span class="fi-calendar"> {{ date("j. n. Y", strtotime(announcement.ann_created)) }}</span></li>
									<li><span class="fi-clock"> {{ date("H:i", strtotime(announcement.ann_created)) }}</span></li>
									{% if auth.isAdmin() %}
										<li> | </li>
										<li><a class="fi-pencil" href="{{ url("announcements/edit/") }}{{ announcement.id_announcement }}"> Upraviť</a> </li>
										<li><a class="fi-trash" href="{{ url("announcements/delete/") }}{{ announcement.id_announcement }}"> Odstrániť</a> </li>
										<li><a class="fi-arrow-up"
											   href="{{ url("announcements/moveUp/") }}{{ announcement.id_announcement }}">
												Topovať</a></li>
                                    {% endif %}
									{% if not auth.isAdmin() and announcement.ann_public %}
										<li> | </li>
									{% endif %}
									{% if announcement.ann_public %}
										<li>{% include "layouts/partials/social-share" with ['announcement': announcement] %}</li>
									{% endif %}
								</ul>
							</div>
						</footer>
					</div>
				</article>
			{% endfor %}
			<div class="pagination-centered">
				<ul class="pagination">
					{% if pagination.current == pagination.first %}
						<li class="arrow unavailable"><a href="{{ url("announcements/list/") }}{{ pagination.current }}">&laquo;</a></li>
					{% else %}
						<li class="arrow"><a href="{{ url("announcements/list/") }}{{ pagination.before }}">&laquo;</a></li>
					{% endif %}
					{% for index in 1..pagination.total_pages %}
						{% if index == pagination.current %}
							<li class="current"><a href="{{ url("announcements/list/") }}{{ index }}">{{ index }}</a></li>
						{% else %}
							<li><a href="{{ url("announcements/list/") }}{{ index }}">{{ index }}</a></li>
						{% endif %}
					{% endfor %}
					{% if pagination.current == pagination.last %}
						<li class="arrow unavailable"><a href="{{ url("announcements/list/") }}{ pagination.current }}">&raquo;</a></li>
					{% else %}
						<li class="arrow"><a href="{{ url("announcements/list/") }}{{ pagination.next }}">&raquo;</a></li>
					{% endif %}
				</ul>
			</div>
		</div>
	</div>
{% endblock %}