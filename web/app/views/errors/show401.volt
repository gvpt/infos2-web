{% extends "layouts/base.volt" %}
{% block title %}Chyba 401{% endblock %}
{% block content %}
    <section class="row">
        <div class="large-12 columns">
            <div class="panel">
                <h2><small class="gray">Prístup bol zamietnutý.</small></h2>
                <p class="text-justify">
                    K tomuto nemáte prístup. Kontaktujte administrátora.
                </p>
                <p>Späť na <a href="{{ url('') }}">domovskú stránku</a>.</p>
            </div>
        </div>
    </section>
{% endblock %}
{% block piwik %}
{% endblock %}