{% extends "layouts/base.volt" %}
{% block title %}Chyba 500{% endblock %}
{% block content %}
    <section class="row">
        <div class="large-12 columns">
            <div class="panel">
                <h2><small class="gray">Interná chyba</small></h2>
                <p class="text-justify">
                    Niečo sa pokazilo, ak sa táto chyba opakuje kontaktujte administrátora.
                </p>
                <p>Späť na <a href="{{ url('') }}">domovskú stránku</a>.</p>
            </div>
        </div>
    </section>
{% endblock %}
{% block piwik %}
{% endblock %}