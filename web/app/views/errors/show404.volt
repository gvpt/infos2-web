{% extends "layouts/base.volt" %}
{% block title %}Chyba 404{% endblock %}
{% block content %}
    <section class="row">
        <div class="large-12 columns">
            <div class="panel">
                <h2><small class="gray">Chyba 404</small></h2>
                <p class="text-justify">Táto stránka neexistuje.</p>
                <p>Späť na <a href="{{ url('') }}">domovskú stránku</a>.</p>
            </div>
        </div>
    </section>
{% endblock %}
{% block piwik %}
{% endblock %}