{% extends "layouts/base.volt" %}
{% block title %}Systemove Nastavenia{% endblock %}
{% block content %}
    <article class="row">
        <div class="large-12 columns">
            <header class="row">
                <div class="large-12 columns"><h2>
                        Title: {{ systemMessage.sysm_title  }}
                    </h2>
                </div>
            </header>
            <div class="row">
                <div class="large-12 columns">
                    {{ systemMessage.sysm_text  }}
                </div>
            </div>
            <hr />
            <footer class="row">
                <div class="large-12 columns">
                    <ul class="inline-list" style="font-size: small;">
                        <li><a href="https://plus.google.com/u/1/{{ systemMessage.id_user }}/about" target="_blank" class="fi-torso"> {{ systemMessage.getUser().getFullName() }}</a></li>
                        <li><span class="fi-calendar"> {{ date("j. n. Y", strtotime(systemMessage.sysm_deadline)) }}</span></li>
                        <li><span class="fi-clock"> {{ date("H:i", strtotime(systemMessage.sysm_deadline)) }}</span></li>
                        <li> | </li>
                        <li><a class="fi-pencil" href="{{ url("system/editSystemMessage/") }}"> Upraviť</a> </li>
                        <li>Aktivne : <span class="label {{ systemMessage.sysm_active ? 'success' : 'alert' }}" style="display: inline">{{ systemMessage.sysm_active ? 'áno' : 'nie' }}</span></li>
                    </ul>
                </div>
            </footer>
        </div>
    </article>
    <article class="row">
        <div class="large-12 columns">
            <header class="row">
                <div class="large-12 columns"><h2>
                        {{ systemNotification.sysm_title  }}
                    </h2>
                </div>
            </header>
            <div class="row">
                <div class="large-12 columns">
                    {{ systemNotification.sysm_text  }}
                </div>
            </div>
            <hr />
            <footer class="row">
                <div class="large-12 columns">
                    <ul class="inline-list" style="font-size: small;">
                        <li><a href="https://plus.google.com/u/1/{{ systemNotification.id_user }}/about" target="_blank" class="fi-torso"> {{ systemNotification.getUser().getFullName() }}</a></li>
                        <li><span class="fi-calendar"> {{ date("j. n. Y", strtotime(systemNotification.sysm_deadline)) }}</span></li>
                        <li><span class="fi-clock"> {{ date("H:i", strtotime(systemNotification.sysm_deadline)) }}</span></li>
                        <li> | </li>
                        <li><a class="fi-pencil" href="{{ url("system/editSystemNotification/") }}"> Upraviť</a> </li>
                        <li>Aktivne : <span class="label {{ systemNotification.sysm_active ? 'success' : 'alert' }}" style="display: inline">{{ systemNotification.sysm_active ? 'áno' : 'nie' }}</span></li>
                    </ul>
                </div>
            </footer>
        </div>
    </article>
{% endblock %}