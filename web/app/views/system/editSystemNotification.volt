{% extends "layouts/base.volt" %}
{% block title %}Upraviť systemovu notifikaciu{% endblock %}
{% block content %}
    <section class="row">
        <div class="large-12 columns">
            <header><h2>Upraviť systemovu notifikaciu</h2></header>
            <form id="formEditSysm" name="formEditSysm" action="{{ url("system/editSystemNotification/") }}" enctype="application/x-www-form-urlencoded" method="post" data-abide>
                <div class="row">
                    <div class="large-6 columns">
                        <h3>Nazov: {{ systemNotification.sysm_title }}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Zobrazovať do
                            <input type="text" id="editSysm_deadline" name="editSysm_deadline" placeholder="Zobrazovať do" required pattern="(0[1-9]|[12][0-9]|3[01])[- \.](0[1-9]|1[012])[- \.](19|20)\d\d" value="{{ date("d.m.Y", strtotime(systemNotification.sysm_deadline)) }}"/>
                            <small class="error">Položka musí obsahovať validný dátum.</small>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <input id="editSysm_active" name="editSysm_active" type="checkbox" value="1" {% if systemNotification.sysm_active %}checked{% endif %}><label for="editSysm_active">Aktivna systemova sprava.</label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label for="editSysm_text">Text</label>
                        <input type="text" id="editSysm_text" name="editSysm_text" placeholder="Text" required value="{{ systemNotification.sysm_text }}"/>
                    </div>
                </div>
                <div class="row">
                    <div class="large-5 large-centered columns">
                        <button type="submit" style="margin-top: 20px;" class="button expand">Odoslať</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
{% endblock %}
{% block javascript %}
    <script type="text/javascript">
        $(document).ready(function() {
            $("#editSysm_deadline").datepicker({
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                dayNamesMin:["Ne", "Po", "Ut", "St", "Št", "Pia", "So"],
                monthNames: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
                minDate: 0
            });
        });
    </script>
{% endblock %}