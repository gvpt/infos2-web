{% extends "layouts/base.volt" %}
{% block title %}Upraviť systemovu spravu{% endblock %}
{% block content %}
    <section class="row">
        <div class="large-12 columns">
            <header><h2>Upraviť systemovu spravu</h2></header>
            <form id="formEditSysm" name="formEditSysm" action="{{ url("system/editSystemMessage/") }}" enctype="application/x-www-form-urlencoded" method="post" data-abide>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Názov
                            <input type="text" id="editSysm_title" name="editSysm_title" placeholder="Názov" required value="{{ systemMessage.sysm_title }}"/>
                        </label>
                        <small class="error">Názov je povinný.</small>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Zobrazovať do
                            <input type="text" id="editSysm_deadline" name="editSysm_deadline" placeholder="Zobrazovať do" required pattern="(0[1-9]|[12][0-9]|3[01])[- \.](0[1-9]|1[012])[- \.](19|20)\d\d" value="{{ date("d.m.Y", strtotime(systemMessage.sysm_deadline)) }}"/>
                            <small class="error">Položka musí obsahovať validný dátum.</small>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <input id="editSysm_active" name="editSysm_active" type="checkbox" value="1" {% if systemMessage.sysm_active %}checked{% endif %}><label for="editSysm_active">Aktivna systemova sprava.</label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label for="editSysm_text">Text</label>
                        <textarea id="editSysm_text" name="editSysm_text" style="height: 400px; width: 100%;">{{ systemMessage.sysm_text }}</textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="large-5 large-centered columns">
                        <button type="submit" style="margin-top: 20px;" class="button expand">Odoslať</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
{% endblock %}
{% block javascript %}
    <script type="text/javascript">
        $(document).ready(function() {
            $("#editSysm_deadline").datepicker({
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                dayNamesMin:["Ne", "Po", "Ut", "St", "Št", "Pia", "So"],
                monthNames: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
                minDate: 0
            });
            tinymce.init({
                selector: "#editSysm_text",
                language : 'sk',
                plugins: ["autolink link charmap anchor hr","searchreplace visualblocks code ","table paste autoresize media image pagebreak"],
                toolbar: "styleselect edit | bold italic underline strikethrough superscript subscript | removeformat | link image media pagebreak hr | table | code",
                entity_encoding : "raw",
                width: "100%",
                menubar: false,
                style_formats: [
                    {title: "Headers", items: [
                        {title: "Header 1", format: "h1"},
                        {title: "Header 2", format: "h2"},
                        {title: "Header 3", format: "h3"},
                        {title: "Header 4", format: "h4"},
                        {title: "Header 5", format: "h5"},
                        {title: "Header 6", format: "h6"}
                    ]},
                    {title: "Inline", items: [
                        {title: "Bold", icon: "bold", format: "bold"},
                        {title: "Italic", icon: "italic", format: "italic"},
                        {title: "Underline", icon: "underline", format: "underline"},
                        {title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
                        {title: "Superscript", icon: "superscript", format: "superscript"},
                        {title: "Subscript", icon: "subscript", format: "subscript"},
                        {title: "Code", icon: "code", format: "code"}
                    ]},
                    {title: "Blocks", items: [
                        {title: "Paragraph", format: "p"},
                        {title: "Blockquote", format: "blockquote"},
                        {title: "Div", format: "div"},
                        {title: "Pre", format: "pre"}
                    ]},
                    {title: "Alignment", items: [
                        {title: "Left", icon: "alignleft", format: "alignleft"},
                        {title: "Center", icon: "aligncenter", format: "aligncenter"},
                        {title: "Right", icon: "alignright", format: "alignright"},
                        {title: "Justify", icon: "alignjustify", format: "alignjustify"}
                    ]}
                ]
            });
        });
    </script>
{% endblock %}