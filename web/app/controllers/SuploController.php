<?php

class SuploController extends ControllerBase {

    public function indexAction() {
		return $this->dispatcher->forward([
			'controller' => 'suplo',
			'action' => 'view',
			'parameter' => date("Y-m-d")
		]);
    }

	public function viewAction($date = '') {
		if (empty($date)) {
			$date = date("Y-m-d");
		}
		$date = new DateTime($date);
		$dateSql = $date->format("Y-m-d");
		$suplo = Suplo::find(array(
			"order" => "sup_nick ASC",
			"conditions"	=> "sup_date = '$dateSql'"
		));
		$this->view->setVar('suplo', $suplo);
		$this->view->setVar('date', $date);
		$this->view->setVar('jqueryui', true);
		$this->view->setVar('momentjs', true);
	}

	public function recordAction($id) {
		if ($this->request->isAjax()) {
			$suploRecord = Suplo::findFirstById_suplo($id);
			if ($suploRecord) {
				$this->view->setVar('record', $suploRecord);
				return true;
			}
			else {
				return $this->response->setStatusCode(404, 'Suplo record not found.');
			}

		}
		else {
			$this->view->disable();
			return $this->response->setStatusCode(403, 'Access forbiden.');
		}
	}

	public function summaryAction($date) {
		$idUser = $this->auth->getIdentity()['id'];
		if($this->request->isAjax()) {
			$suplo = Suplo::find(array(
				"conditions"	=> "DATE_FORMAT(sup_date, '%m-%Y') = '$date' AND id_user = '$idUser'"
			));
			$this->view->setVar('suplo', $suplo);
		}
		else {
			$suplo = Suplo::find(array(
				"conditions"	=> "DATE_FORMAT(sup_date, '%m-%Y') = '$date' AND id_user = '$idUser'"
			));
			$date = DateTime::createFromFormat('m-Y', $date);
			$this->view->setVar('season', $date);
			$this->view->setVar('suplo', $suplo);
			$this->view->pick('suplo/printSummary');
		}
	}

	public function newAction() {
		$this->view->setVar('date', date("d.m.Y"));
		if ($this->request->isPost()) {
			$date = new DateTime($this->request->getPost('newSuplo_date'));
			$date = $date->format("Y-m-d");
			$input = $this->request->getPost('newSuplo_text');
			$suplo = Suplo::find([
				"conditions"	=> "sup_date = '$date'"
			]);
			$this->db->begin();
			foreach ($suplo as $record) {
				if(!$record->delete()) {
					$this->db->rollback();
					$this->flash->error("Nepodarilo sa vytvoriť suplovanie na deň $date. Skúste prosím neskôr.");
					$error = "User " . $this->auth->getName() . " has failed to create substitution for $date. \n Errors: \n";
					foreach($record->getMessages() as $message) {
						$error .= $message . "\n ";
					}
					$this->logger->error($error);
					return $this->response->redirect('suplo/new');
				}
			}
			foreach(preg_split("/((\r?\n)|(\r\n?))/", $input) as $key => $line){
				if ($key != 0) {
					$record = array();
					foreach (explode("\t", $line) as $cell) {
						$record[] = $cell;
					}
					if (!empty($record[0])) {
						$suploRecord = new Suplo;
						$suploRecord->sup_date = $date;
						if (!$suploRecord->setHour($record[0])) {
							$this->db->rollback();
							$this->flash->error("Nepodarilo sa vytvoriť suplovanie na deň $date. Skúste prosím neskôr.");
							return $this->response->redirect('suplo/new');
						}
						$suploRecord->sup_nick = $record[1];
						$suploRecord->sup_classes = $record[2];
						$suploRecord->sup_subject = $record[3];
						if (count($record) == 6) {
							$suploRecord->sup_classroom = 'n/a';
							$success = $suploRecord->setUser($record[4]);
							$suploRecord->sup_note = $record[5];
						}
						else {
							$suploRecord->sup_classroom = $record[4];
							$success = $suploRecord->setUser($record[5]);
							$suploRecord->sup_note = $record[6];
						}
						if (!$success) {
							$this->db->rollback();
							$this->flash->error("Nepodarilo sa vytvoriť suplovanie na deň $date. Skúste prosím neskôr.");
							return $this->response->redirect('suplo/new');
						}
						try {
							if(!$suploRecord->save()) {
								$this->db->rollback();
								$this->flash->error("Nepodarilo sa vytvoriť suplovanie na deň $date. Skúste prosím neskôr.");
								$error = "User " . $this->auth->getName() . " has failed to create substitution for $date. \nErrors: \n";
								foreach($suploRecord->getMessages() as $message) {
									$error .= $message . "\n";
								}
								$error .= print_r($record, true) . "\n";
								$this->logger->error($error);
								return $this->response->redirect('suplo/new');
							}
						}
						catch (Exception $e) {
							$this->flash->error("Nepodarilo sa vytvoriť suplovanie na deň $date. Skúste prosím neskôr.");
							$error = "User " . $this->auth->getName() . " has failed to create substitution for $date. \nErrors: \n";
							$error .= $e->getMessage();
							$error .= print_r($record, true);
							$this->logger->error($error);
							return $this->response->redirect('suplo/new');
						}
					}
				}
			}
			try {
				$this->db->commit();
			}
			catch (Exception $e) {
				$this->flash->error("Nepodarilo sa vytvoriť suplovanie na deň $date. Skúste prosím neskôr.");
				$error = "User " . $this->auth->getName() . " has failed to create substitution for $date. \nErrors: \n";
				$error .= $e->getMessage();
				$this->logger->error($error);
				return $this->response->redirect('suplo/new');
			}
			$this->flash->success("Suplovanie na deň $date bolo úspešne vytvorené/zmenené.");
			$this->logger->info("User " . $this->auth->getName() . " has created/updated substitution for $date.");
			return $this->response->redirect('suplo');
		}
		else {
			$this->view->setVar('jqueryui', true);
			return true;
		}
	}

	public function existsAction($date) {
		$this->view->disable();
		if ($this->request->isAjax()) {
			$date = new DateTime($date);
			$suplo = Suplo::findFirstBySup_date($date->format("Y-m-d"));
			if ($suplo) {
				$result = [
					'exists' => true,
					'text' => 'Suplovanie na dátum ' . $date->format('j. n. Y') . ' už existuje. Prepisujem.'
				];
				$result = json_encode($result);
			}
			else {
				$result = [
					'exists' => false
				];
				$result = json_encode($result);
			}
			echo $result;

		}
	}

}

