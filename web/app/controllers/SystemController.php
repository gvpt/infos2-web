<?php

class SystemController extends ControllerBase {

    public function indexAction() {
        $systemMessage = SystemMessage::findFirst(array(
            "conditions"	=> "sysm_type = 0"
        ));
        if ($systemMessage) {
            $this->view->setVar('systemMessage', $systemMessage);
        } else {
            $this->view->setVar('systemMessage', false);
        }
        $systemNotification = SystemMessage::findFirst(array(
            "conditions"	=> "sysm_type = 1"
        ));
        if ($systemNotification) {
            $this->view->setVar('systemNotification', $systemNotification);
        } else {
            $this->view->setVar('systemNotification', false);
        }
    }
    
    public function editSystemMessageAction() {
        /**
         * @var SystemMessage $systemMessage
         */
        $systemMessage = SystemMessage::findFirst(array(
            "conditions"	=> "sysm_type = 0"
        ));
        if ($systemMessage) {
            if ($this->request->isPost() == true) {
                $systemMessage->sysm_updated = date("Y-m-d H:i:s");
                $systemMessage->sysm_title = $this->request->getPost('editSysm_title');
                $systemMessage->sysm_text = $this->request->getPost('editSysm_text');
                $systemMessage->id_user = $this->auth->getIdentity()['id'];
                $deadline = new DateTime($this->request->getPost('editSysm_deadline'));
                $systemMessage->sysm_deadline = $deadline->format("Y-m-d");
                if($this->request->getPost('editSysm_active')) {
                    $systemMessage->sysm_active = $this->request->getPost('editSysm_active');
                } else {
                    $systemMessage->sysm_active = 0;
                }
                if ($systemMessage->update()) {
                    $this->logger->info("User " . $this->auth->getName() . " updated systemMessage " . $systemMessage->sysm_title);
                    return $this->response->redirect('system');
                }
                else {
                    $this->view->setVar("systemMessage", $systemMessage);
                    $this->flash->error("Nepodarilo sa upraviť oznam " . $systemMessage->sysm_title . ". Skúste prosím neskôr.");
                    $this->logger->error("User " . $this->auth->getName() . " has failed to update systemMessage " . $systemMessage->sysm_title);
                    return true;
                }
            }
            else {
                $this->view->setVar("systemMessage", $systemMessage);
                $this->view->setVar("tinymce", true);
                $this->view->setVar("jqueryui", true);
                return true;
            }
        }
        else {
            return $this->response->redirect('errors/show404');
        }
    }

    public function editSystemNotificationAction() {
        /**
         * @var SystemMessage $systemNotification
         */
        $systemNotification = SystemMessage::findFirst(array(
            "conditions"	=> "sysm_type = 1"
        ));
        if ($systemNotification) {
            if ($this->request->isPost() == true) {
                $systemNotification->sysm_updated = date("Y-m-d H:i:s");
                $systemNotification->sysm_text = $this->request->getPost('editSysm_text');
                $systemNotification->id_user = $this->auth->getIdentity()['id'];
                $deadline = new DateTime($this->request->getPost('editSysm_deadline'));
                $systemNotification->sysm_deadline = $deadline->format("Y-m-d");
                if($this->request->getPost('editSysm_active')) {
                    $systemNotification->sysm_active = $this->request->getPost('editSysm_active');
                } else {
                    $systemNotification->sysm_active = 0;
                }
                if ($systemNotification->update()) {
                    $this->logger->info("User " . $this->auth->getName() . " updated systemNotification " . $systemNotification->sysm_title);
                    return $this->response->redirect('system');
                }
                else {
                    $this->view->setVar("systemNotification", $systemNotification);
                    $this->flash->error("Nepodarilo sa upraviť oznam " . $systemNotification->sysm_title . ". Skúste prosím neskôr.");
                    $this->logger->error("User " . $this->auth->getName() . " has failed to update systemNotification " . $systemNotification->sysm_title);
                    return true;
                }
            }
            else {
                $this->view->setVar("systemNotification", $systemNotification);
                $this->view->setVar("jqueryui", true);
                return true;
            }
        }
        else {
            return $this->response->redirect('errors/show404');
        }
    }

}

