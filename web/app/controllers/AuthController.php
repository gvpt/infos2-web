<?php

class AuthController extends ControllerBase {

    public function indexAction() {
		$this->response->redirect($this->auth->createLoginUrl());
    }
	
    public function loginAction() {
		$code = $this->request->get('code');
		if($code) {
			require_once (__DIR__.'/../../vendor/autoload.php');
			$client = new Google_Client();
			$client->setClientId($this->config->google->clientId);
			$client->setClientSecret($this->config->google->clientSecret);
			$client->setRedirectUri($this->config->application->baseUri . "auth/login");
			$client->setAccessType("offline");
			$client->addScope("email");
			$client->addScope("profile");
			$tokens = $client->authenticate($code);
			if(!isset($tokens->error)) {
				$data = json_decode($tokens);
				$credentials = array(
					'token' => array(
						'access' => $data->access_token,
					)
				);
				$data = $client->verifyIdToken($data->id_token)->getAttributes();
				if(!isset($data->error)) {
					$ticket = $data['payload'];
					$credentials['id'] = $ticket['sub'];
					$credentials['email'] = $ticket['email'];
					$credentials['token']['issued'] = $ticket['iat'];
					$credentials['token']['expires'] = $ticket['exp'];
					if($this->auth->authenticate($credentials)) {
						return $this->response->redirect('dashboard');
					} else {
						$this->flash->error('Užívateľ nenájdený, skúste znova s iným google kontom.');
						return $this->response->redirect();
					}
				} else {
					$this->logger->error("Error while verifyIdToken: " . $data->error);
					$this->flash->error('Nastala chyba pri prihlasovaní. Skúste znova.');
					return $this->response->redirect();
				}
			} else {
				$this->logger->error("Error while exchanging code for tokens: " . $tokens->error);
				$this->flash->error('Nastala chyba pri prihlasovaní. Skúste znova.');
				return $this->response->redirect();
			}
		} else {
			$this->logger->error("Code is missing in the request");
			$this->flash->error('Nastala chyba pri prihlasovaní. Skúste znova.');
			return $this->response->redirect();
		}
    }

	public function logoutAction() {
		$this->auth->remove();
		return $this->response->redirect();
	}

	public function logoutGoogleAction() {
		$this->auth->remove();
		return $this->response->redirect('https://mail.google.com/mail/u/0/?logout&hl=en');
	}

}

//TODO:: Implement CSRF protection