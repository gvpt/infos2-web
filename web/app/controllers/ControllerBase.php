<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends Controller {

	public function beforeExecuteRoute(Dispatcher $dispatcher) {
		$controllerName = $dispatcher->getControllerName();

		$systemNotification = SystemMessage::findFirst(array(
			"conditions"	=> "sysm_type = 1 AND sysm_deadline >= CURDATE()"
		));
		if ($systemNotification) {
			$this->view->setVar('systemNotification', $systemNotification);
		} else {
			$this->view->setVar('systemNotification', false);
		}

		if ($this->acl->isPrivate($controllerName)) {
			if (!$this->auth->isLoggedIn()) {
				return $this->response->redirect('errors/show401');
			}

			$actionName = $dispatcher->getActionName();
			if (!$this->acl->isAllowed($this->auth->getIdentity()['role'], $controllerName, $actionName)) {
				$this->flash->error("You don't have access to this module: " . $controllerName . '/' . $actionName);

				if ($this->acl->isAllowed($this->auth->getIdentity()['role'], $controllerName, 'index')) {
					$dispatcher->forward(array(
						'controller' => $controllerName,
						'action' => 'index'
					));
				}
				else {
                    return $this->response->redirect('errors/show401');
                }

				return false;
			}
		}
	}

	protected function bgExec($cmd) {
		if(substr(php_uname(), 0, 7) == "Windows"){
			pclose(popen("start /B ". $cmd, "r"));
		}else {
			exec($cmd . " > /dev/null &");
		}
	}

	protected function execInfos2Cli($params) {
		$app = $this->config->cli->dir . 'cli.php ' . $params;
		$this->bgExec("php -q $app");
	}
}
