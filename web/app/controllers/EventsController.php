<?php

class EventsController extends ControllerBase {

    public function indexAction() {
		return $this->dispatcher->forward(array(
			'controller' => 'events',
			'action' => 'list',
			'parameter' => 1
		));
    }

	public function viewAction($id) {
		$event = Event::findFirstByid_event($id);
		if ($event) {
			$this->view->setVar('event', $event);
			return true;
		}
		else {
			return $this->response->redirect('errors/show404');
		}

	}

	public function newAction($type) {
		if ($this->request->isPost()) {
			$event = new Event;
			$event->evt_created = date('Y-m-d H:i:s');
			$event->id_eventType = $this->request->getPost('newEvent_type');
			$event->id_user = $this->auth->getIdentity()['id'];
			$event->evt_title = $this->request->getPost('newEvent_title');
			if ($this->request->getPost('newEvent_place')) {
                $event->evt_place = $this->request->getPost('newEvent_place');
            } else {
                $event->evt_place = "Neurčené";
            }
			$evtDate = new Datetime($this->request->get('newEvent_date'));
			$event->evt_date = $evtDate->format("Y-m-d");
            if ($this->request->getPost('newEvent_start')) {
                $event->evt_startTime = $this->request->getPost('newEvent_start');
            }
            $event->evt_uid = hash('sha256',time() . openssl_random_pseudo_bytes(10));
			if ($this->request->getPost('newEvent_end')) {
				$event->evt_endTime = $this->request->getPost('newEvent_end');
			}
			$event->evt_description = $this->request->getPost('newEvent_description');
            if ($this->request->getPost('newEvent_public')) {
                $event->evt_public = 1;
            } else {
                $event->evt_public = 0;
            }
            if ($this->request->getPost('newEvent_infos')) {
                $event->evt_infos = 1;
            } else {
                $event->evt_infos = 0;
            }
            if ($this->request->getPost('newEvent_shortDescription')) {
                $event->evt_shortDescription = $this->request->getPost('newEvent_shortDescription');
            }
            if ($this->request->getPost('newEvent_program')) {
				$event->evt_program = $this->request->getPost('newEvent_program');
			}
			if ($event->create()) {
				$this->logger->info("User " . $this->auth->getName() . " created event " . $event->evt_title);
				return $this->response->redirect('events/view/' . $event->id_event);
			}
			else {
				$eventType = EventType::findFirstByEvyType($type);
				$this->view->setVar('eventType', $eventType);
				$this->flash->error("Nepodarilo sa vytvoriť udalosť " . $event->evt_title . ". Skúste prosím neskôr.");
				$this->logger->error("User " . $this->auth->getName() . " has failed to create event " . $event->evt_title);
                return true;
			}

		} else {
			$this->view->setVar('eventType', EventType::findFirstByEvyType($type));
            $this->view->setVar('places', Place::find());
			switch ($type) {
				case 'meeting':
					$this->view->setVar('program', true);
					$this->view->setVar('endTime', true);
					break;
				case 'school':
					$this->view->setVar('endTime', true);
					break;
				case 'casual':
					$this->view->setVar('endTime', true);
					break;

			}
			$this->view->setVar('tinymce', true);
			$this->view->setVar('jqueryui', true);
			$this->view->setVar('timepicker', true);
            $this->view->setVar('eventjs', true);
            return true;
		}
	}


	public function listAction($offset = 1) {
		$event = Event::find(array(
            "order" => "evt_date DESC"
		));
		$paginator = new \Phalcon\Paginator\Adapter\Model(
			array(
				"data" => $event,
				"limit"=> 20,
				"page" => $offset
			)
		);
		$this->view->setVar('pagination', $paginator->getPaginate());
		$this->view->setVar('offset', $offset);
	}

	public function editAction($id) {
		/**
		 * @var Event $event
		 */
		$event = Event::findFirstByid_event($id);
		if ($event) {
			if ($this->request->isPost() == true) {
				$event->id_user = $this->auth->getIdentity()['id'];
				$event->evt_updated = date("Y-m-d H:i:s");
				$eventDate = new DateTime($this->request->getPost('editEvent_date'));
				$event->evt_date = $eventDate->format("Y-m-d");
                if ($this->request->getPost('editEvent_start')) {
                    $event->evt_startTime = $this->request->getPost('editEvent_start');
                }
				if ($this->request->getPost('editEvent_end')) {
					$event->evt_endTime = $this->request->getPost('editEvent_end');
				}
				$event->evt_title = $this->request->getPost('editEvent_title');
                if ($this->request->getPost('editEvent_place')) {
                    $event->evt_place = $this->request->getPost('editEvent_place');
                } else {
                    $event->evt_place = "Neurčené";
                }
                if ($this->request->getPost('editEvent_public')) {
                    $event->evt_public = 1;
                } else {
                    $event->evt_public = 0;

                }
                if ($this->request->getPost('editEvent_infos')) {
                    $event->evt_infos = 1;
                } else {
                    $event->evt_infos = 0;
                }
                if ($this->request->getPost('editEvent_shortDescription')) {
                    $event->evt_shortDescription = $this->request->getPost('editEvent_shortDescription');
                }
				if ($this->request->getPost('editEvent_program')) {
					$event->evt_program = $this->request->getPost('editEvent_program');
				}
				$event->evt_description = $this->request->getPost('editEvent_description');
				if ($event->update()) {
					$this->logger->info("User " . $this->auth->getName() . " updated event " . $event->evt_title);
					return $this->response->redirect('events/view/' . $event->id_event);
				}
				else {
					$this->view->setVar("event", $event);
					$this->view->setVar('places', Place::find());
					$this->view->setVar("tinymce", true);
					$this->view->setVar("jqueryui", true);
					$this->view->setVar('timepicker', true);
                    $this->view->setVar("eventjs", true);
                    $this->flash->error("Nepodarilo sa upraviť udalosť " . $event->evt_title . ". Skúste prosím neskôr.");
					$this->logger->error("User " . $this->auth->getName() . " has failed to update event " . $event->evt_title);
					return true;
				}
			}
			else {
				$this->view->setVar("event", $event);
				$this->view->setVar('places', Place::find());
				$this->view->setVar("tinymce", true);
				$this->view->setVar("jqueryui", true);
				$this->view->setVar('timepicker', true);
                $this->view->setVar("eventjs", true);
                return true;
			}
		}
		else {
			return $this->response->redirect('errors/show404');
		}
	}

	public function deleteAction($id) {
		/**
		 * @var Event $event
		 */
		$event = Event::findFirstByid_event($id);
		if ($event) {
			if ($event->delete()) {
				$this->logger->info("User " . $this->auth->getName() . " deleted event " . $event->evt_title);
				$this->flash->success("Udalosť bola úspešne zmazaná.");
				return $this->response->redirect('events');
			}
			else {
				$this->flash->error("Nepodarilo sa odstániť udalosť " . $event->evt_title . ". Skúste prosím neskôr.");
				$this->logger->error("User " . $this->auth->getName() . " has failed to delete event " . $event->evt_title);
				return $this->response->redirect('events/view/' . $id);
			}
		}
		else {
			return $this->response->redirect('errors/show404');
		}

	}


}

