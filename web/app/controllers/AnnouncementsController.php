<?php

class AnnouncementsController extends ControllerBase {

    public function indexAction() {
        return $this->dispatcher->forward(array(
            'controller' => 'announcements',
            'action' => 'list',
            'parameter' => 1
        ));
    }

    public function newAction() {
        if ($this->request->isPost() == true) {
            $announcement = new Announcement();
            $announcement->ann_created = date("Y-m-d H:i:s");
            $announcement->ann_title = $this->request->getPost('newAnn_title');
            $announcement->ann_text = $this->request->getPost('newAnn_text');
            $announcement->id_user = $this->auth->getIdentity()['id'];
            $deadline = new DateTime($this->request->getPost('newAnn_deadline'));
            $announcement->ann_deadline = $deadline->format("Y-m-d");
            $announcement->ann_uid = hash('sha256',time() . openssl_random_pseudo_bytes(10));
            if($this->request->getPost('newAnn_public')) {
                $announcement->ann_public = true;
            }
            if ($announcement->create()) {
                $this->logger->info("User " . $this->auth->getName() . " created announcement " . $announcement->ann_title);
                if ($announcement->ann_public) {
                    $this->newArticle($announcement);
                }
                return $this->response->redirect('announcements/view/' . $announcement->id_announcement);
            } else {
                $this->flash->error("Nepodarilo sa vytvoriť oznam " . $announcement->ann_title . ". Skúste prosím neskôr.");
                $this->logger->error("User " . $this->auth->getName() . " has failed to create announcement " . $announcement->ann_title);
                return $this->response->redirect('announcements');
            }
        } else {
            $this->view->setVar("tinymce", true);
            $this->view->setVar("jqueryui", true);
            return true;
        }
    }

    protected function newArticle($announcement)
    {
        $article = new Article();
        $article->catid = $this->config->application->catId;
        $article->title = $announcement->ann_title;
        $article->introtext = $announcement->ann_text;
        $deadline = new DateTime($announcement->ann_deadline);
        $deadline->setTimezone(new DateTimeZone("UTC"));
        $article->publish_down = $deadline->format("Y-m-d H:i:s");
        if ($article->create()) {
            $announcement->ann_foreignerId = $article->id;
            $announcement->ann_public = 1;
            $announcement->update();
            $this->logger->info("User " . $this->auth->getName() . " created announcement on School web " . $announcement->ann_title);
            return true;
        } else {
            $this->flash->error("Oznam v infose sa vytvoril ale nepodarilo sa ho vytvorit na skolskom webe " . $announcement->ann_title . ". Skúste prosím neskôr.");
            $this->logger->error("User " . $this->auth->getName() . " has failed to create announcement on School web " . $announcement->ann_title);
            return true;
        }
    }

    public function editAction($id) {
        /**
         * @var Announcement $announcement
         */
        $announcement = Announcement::findFirstByid_announcement($id);
        if ($announcement) {
            if ($this->request->isPost() == true) {
                $announcement->ann_updated = date("Y-m-d H:i:s");
                $announcement->ann_title = $this->request->getPost('editAnn_title');
                $announcement->ann_text = $this->request->getPost('editAnn_text');
                $announcement->id_user = $this->auth->getIdentity()['id'];
                $deadline = new DateTime($this->request->getPost('editAnn_deadline'));
                $announcement->ann_deadline = $deadline->format("Y-m-d");
                $wasPublic = $announcement->ann_public;
                if ($this->request->getPost('editAnn_public')) {
                    $announcement->ann_public = $this->request->getPost('editAnn_public');
                } else {
                    $announcement->ann_public = 0;
                }
                if ($announcement->update()) {
                    if ($wasPublic) {
                        $this->logger->info("User " . $this->auth->getName() . " updated announcement " . $announcement->ann_title);
                        if ($announcement->ann_public) {
                            $this->editArticle($announcement);
                        } else {
                            $this->unPublishArticle($announcement);
                        }
                        return $this->response->redirect('announcements/view/' . $announcement->id_announcement);
                    } else {
                        if ($announcement->ann_public) {
                            if ($announcement->ann_foreignerId) {
                                $this->editArticle($announcement);
                                $this->publishArticle($announcement);
                            } else {
                                $this->newArticle($announcement);
                            }
                        }
                        return $this->response->redirect('announcements/view/' . $announcement->id_announcement);
                    }
                } else {
                    $this->view->setVar("announcement", $announcement);
                    $this->flash->error("Nepodarilo sa upraviť oznam " . $announcement->ann_title . ". Skúste prosím neskôr.");
                    $this->logger->error("User " . $this->auth->getName() . " has failed to update announcement " . $announcement->ann_title);
                    return true;
                }
            } else {
                $this->view->setVar("announcement", $announcement);
                $this->view->setVar("tinymce", true);
                $this->view->setVar("jqueryui", true);
                return true;
            }
        } else {
            return $this->response->redirect('errors/show404');
        }
    }

    protected function editArticle($announcement) {
        $article = Article::findFirstByid($announcement->ann_foreignerId);
        $article->title = $announcement->ann_title;
        $article->introtext = $announcement->ann_text;
        $deadline = new DateTime($announcement->ann_deadline);
        $deadline->setTimezone(new DateTimeZone("UTC"));
        $article->publish_down = $deadline->format("Y-m-d H:i:s e");
        if ($article->update()) {
            $this->view->setVar("announcement", $announcement);
            $this->logger->info("User " . $this->auth->getName() . " updated announcement on school web " . $announcement->ann_title);
            return true;
        } else {
            $this->view->setVar("announcement", $announcement);
            $this->flash->error("Nepodarilo sa upraviť oznam na skolskom webe " . $announcement->ann_title . ". Skúste prosím neskôr.");
            $this->logger->error("User " . $this->auth->getName() . " has failed to update announcement on school web " . $announcement->ann_title);
            return true;
        }
    }

    protected function unPublishArticle($announcement) {
        $article = Article::findFirstByid($announcement->ann_foreignerId);
        $article->state = -2;
        if ($article->update()) {
            $this->view->setVar("announcement", $announcement);
            $this->logger->info("User " . $this->auth->getName() . " updated announcement on school web " . $announcement->ann_title);
            return true;
        } else {
            $this->view->setVar("announcement", $announcement);
            $this->flash->error("Nepodarilo sa upraviť oznam na skolskom webe " . $announcement->ann_title . ". Skúste prosím neskôr.");
            $this->logger->error("User " . $this->auth->getName() . " has failed to update announcement on school web " . $announcement->ann_title);
            return true;
        }
    }

    protected function publishArticle($announcement) {
        $article = Article::findFirstByid($announcement->ann_foreignerId);
        $article->state = 1;
        if ($article->update()) {
            $this->view->setVar("announcement", $announcement);
            $this->logger->info("User " . $this->auth->getName() . " updated announcement on school web " . $announcement->ann_title);
            return true;
        } else {
            $this->view->setVar("announcement", $announcement);
            $this->flash->error("Nepodarilo sa upraviť oznam na skolskom webe " . $announcement->ann_title . ". Skúste prosím neskôr.");
            $this->logger->error("User " . $this->auth->getName() . " has failed to update announcement on school web " . $announcement->ann_title);
            return true;
        }
    }

    public function deleteAction($id)
    {
        /**
         * @var Announcement $announcement
         */
        $announcement = Announcement::findFirstByid_announcement($id);
        if ($announcement) {
            if ($announcement->ann_public) {
                $this->unPublishArticle($announcement);
            }
            if ($announcement->delete()) {
                $this->logger->info("User " . $this->auth->getName() . " deleted announcement " . $announcement->ann_title);
                $this->flash->success("Oznam bol úspešne zmazaný.");
                return $this->response->redirect('announcements');
            } else {
                $this->flash->error("Nepodarilo sa odstániť oznam " . $announcement->ann_title . ". Skúste prosím neskôr.");
                $this->logger->error("User " . $this->auth->getName() . " has failed to delete announcement " . $announcement->ann_title);
                return $this->response->redirect('announcements/view/' . $id);
            }
        } else {
            return $this->response->redirect('errors/show404');
        }

    }

    public function viewAction($id)
    {
        $announcement = Announcement::findFirstByid_announcement($id);
        if ($announcement) {
            $this->view->setVar('announcement', $announcement);
            return true;
        } else {
            return $this->response->redirect('errors/show404');
        }
    }

    public function listAction($offset = 1)
    {
        $announcements = Announcement::find(array(
            "order" => "ann_created DESC"
        ));
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $announcements,
                "limit" => 10,
                "page" => $offset
            )
        );
        $this->view->setVar('pagination', $paginator->getPaginate());
        $this->view->setVar('offset', $offset);
    }

    public function moveUpAction($id)
    {
        /**
         * @var Announcement $announcement
         */
        $announcement = Announcement::findFirstByid_announcement($id);
        if ($announcement) {
            $announcement->ann_updated = date("Y-m-d H:i:s");
            $announcement->ann_created = date("Y-m-d H:i:s");
            $announcement->id_user = $this->auth->getIdentity()['id'];
            $wasPublic = $announcement->ann_public;
            if ($announcement->update()) {
                if ($wasPublic) {
                    $this->logger->info("User " . $this->auth->getName() . " updated announcement " . $announcement->ann_title);
                    if ($announcement->ann_public) {
                        $this->editArticle($announcement);
                    } else {
                        $this->unPublishArticle($announcement);
                    }
                    return $this->response->redirect('announcements/view/' . $announcement->id_announcement);
                } else {
                    if ($announcement->ann_public) {
                        if ($announcement->ann_foreignerId) {
                            $this->editArticle($announcement);
                            $this->publishArticle($announcement);
                        } else {
                            $this->newArticle($announcement);
                        }
                    }
                    return $this->response->redirect('announcements/view/' . $announcement->id_announcement);
                }
            } else {
                $this->view->setVar("announcement", $announcement);
                $this->flash->error("Nepodarilo sa upraviť oznam " . $announcement->ann_title . ". Skúste prosím neskôr.");
                $this->logger->error("User " . $this->auth->getName() . " has failed to update announcement " . $announcement->ann_title);
                return true;
            }
        } else {
            return $this->response->redirect('errors/show404');
        }
    }

}