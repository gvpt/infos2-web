<?php

class DashboardController extends ControllerBase {

    public function indexAction() {
		$userId = $this->auth->getIdentity()['id'];

		//UserPanel
		$sql = "SELECT * FROM timetable WHERE CAST(NOW() as time) BETWEEN tmt_startTime AND tmt_endTime;";
		$current = $this->db->fetchOne($sql, Phalcon\Db::FETCH_OBJ);
		if ($current->tmt_label == 'Neprebieha výuka') {
			$next = $this->getNextLesson(0);
		}
		else {
			$next = $this->getNextLesson($current->id_timetable);
		}
		$this->view->setVar('current', $current);
		$this->view->setVar('next', $next);



		//Announcements
		$announcements = Announcement::find(array(
			"conditions"	=> "ann_deadline >= CURDATE()",
			"limit"			=> 7,
			"order"			=> "ann_created DESC"
		));
		$this->view->setVar("announcements", $announcements);

		//Events
		$eventTypes = EventType::find();
		$this->view->setVar("eventTypes", $eventTypes);
		$events = Event::find(array(
			"conditions"	=> "evt_date >= CURDATE() AND evt_infos = 1",
			"limit"			=> 5,
			"order"			=> "evt_date ASC"
		));
		$this->view->setVar("events", $events);

		//Suplo
		$date = date('Y-m-d');
		$suploUserToday = Suplo::find(array(
			"conditions"	=> "sup_date = '$date' AND id_user = '$userId'"
		));
		$this->view->setVar("suploUserToday", $suploUserToday);
		$date = date('Y-m-d', strtotime('tomorrow'));
		$suploUserTomorrow = Suplo::find(array(
			"conditions"	=> "sup_date = '$date' AND id_user = '$userId'"
		));
		$this->view->setVar("suploUserTomorrow", $suploUserTomorrow);

		//SuploHistory
		$session = date("m-Y");
		$suploHistory = Suplo::find(array(
			"conditions"	=> "DATE_FORMAT(sup_date, '%m-%Y') = '$session' AND id_user = '$userId'",
			"order"			=> "sup_date DESC"
		));
		$this->view->setVar('suploHistory', $suploHistory);
		$sql = "SELECT DISTINCT DATE_FORMAT(sup_date, '%m-%Y') AS season_value, sup_date, DATE_FORMAT(sup_date, '%M / %Y') AS season_title  FROM suplo WHERE id_user = '$userId' ORDER BY sup_date DESC LIMIT 9";
		$suploSeasons = $this->db->fetchAll($sql, Phalcon\Db::FETCH_OBJ);
		$this->view->setVar('suploSeasons', $suploSeasons);

		$this->view->setVar('refresh', true);
    }

	public function welcomeAction() {

	}

	private function getNextLesson($currentId) {
		$idNext = $currentId + 1;
		$sql = "SELECT * FROM timetable WHERE id_timetable = $idNext";
		try {
			$result = $this->db->fetchOne($sql, Phalcon\Db::FETCH_OBJ);
			if ($result) {
				return $result;
			}
			else {
				$sql = "SELECT * FROM timetable WHERE id_timetable = 1";
				$result = $this->db->fetchOne($sql, Phalcon\Db::FETCH_OBJ);
				return $result;
			}
		}
		catch (Exception $e) {
			return false;
		}
	}
}

