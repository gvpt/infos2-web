<?php

class SettingsController extends ControllerBase {

    public function indexAction() {
		//Newsletter
		$idUser = $this->auth->getIdentity()['id'];
		$newsletter = Newsletter::find(array(
			"conditions"	=> "id_user = '$idUser'",
		));
		$this->view->setVar('newsletter', $newsletter);
		$setting = Setting::find(array(
			"conditions"	=> "id_user = '$idUser'",
		));
		$this->view->setVar('setting', $setting);
	}

	public function newNewsletterAction() {
		if ($this->request->isPost()) {
			$newsletter = new Newsletter();
			$newsletter->id_user = $this->auth->getIdentity()['id'];
			$newsletter->nws_email = $this->request->getPost('newNewsletter_email');
			if ($this->request->getPost('newNewsletter_announcements')) {
				$newsletter->nws_announcements = 1;
			}
			else {
				$newsletter->nws_announcements = 0;
			}
			if ($this->request->getPost('newNewsletter_events')) {
				$newsletter->nws_events = 1;
			}
			else {
				$newsletter->nws_events = 0;
			}
			$newsletter->setSuplo($this->request->getPost('newNewsletter_suplo'));
			if ($newsletter->create()) {
				$this->flash->success('E-mail bol úspešne zapísaný');
				$this->logger->info('User ' . $this->auth->getName() . ' has added record id ' . $newsletter->id_newsletter . ' to newsletter.');
				return $this->response->redirect('settings');
			}
			else {
				$this->flash->error('Pri zapisovaní nastala chyba.');
				$this->logger->info('User ' . $this->auth->getName() . ' has failed to add record to newsletter.');
				return $this->response->redirect('settings');
			}
		}
		else {
			return true;
		}
	}

	public function editNewsletterAction($id) {
		/**
		 * @var Newsletter $newsletter
		 */
		$newsletter = Newsletter::findFirstById_newsletter($id);
		if ($newsletter) {
			$this->view->setVar('newsletter', $newsletter);
			if ($this->request->isPost()) {
				if ($this->request->getPost('editNewsletter_announcements')) {
					$newsletter->nws_announcements = 1;
				}
				else {
					$newsletter->nws_announcements = 0;
				}
				if ($this->request->getPost('editNewsletter_events')) {
					$newsletter->nws_events = 1;
				}
				else {
					$newsletter->nws_events = 0;
				}
				$newsletter->setSuplo($this->request->getPost('editNewsletter_suplo'));
				if ($newsletter->update()) {
					$this->flash->success('Záznam bol úspešne upravený.');
					$this->logger->info('User ' . $this->auth->getName() . " has updated record id $id in newsletter.");
					return $this->response->redirect('settings');
				}
				else {
					$this->flash->error('Pri upravovaní nastala chyba.');
					$this->logger->info('User ' . $this->auth->getName() . " has failed to update record $id in newsletter.");
					return $this->response->redirect('settings');
				}
			}
			else {
				return true;
			}
		}
		else {
			return $this->response->redirect('show404');
		}
	}

	public function deleteNewsletterAction($id) {
		/**
		 * @var Newsletter $newsletter
		 */
		$newsletter = Newsletter::findFirstById_newsletter($id);
		if ($newsletter) {
			if ($newsletter->delete()) {
				$this->flash->success('Záznam bol úspešne odstránený.');
				$this->logger->info('User ' . $this->auth->getName() . ' has deleted record id ' . $newsletter->id_newsletter . ' in newsletter.');
				return $this->response->redirect('settings');
			}
			else {
				$this->flash->error('Pri odstraňovaní nastala chyba.');
				$this->logger->info('User ' . $this->auth->getName() . " has failed to delete record $id in newsletter.");
				return $this->response->redirect('settings');
			}
		}
		else {
			return $this->response->redirect('errors/show404');
		}
	}

	public function editSettingAction($id) {
		/**
		 * @var Setting $setting
		 */
		$setting = Setting::findFirstById_setting($id);
		if ($setting) {
			$this->view->setVar('setting', $setting);
			if ($this->request->isPost()) {
				if ($this->request->getPost('editSetting_logoutGoogle')) {
					$setting->set_logoutGoogle = 1;
				}
				else {
					$setting->set_logoutGoogle = 0;
				}
				if ($setting->update()) {
					$this->flash->success('Záznam bol úspešne upravený.');
					$this->logger->info('User ' . $this->auth->getName() . " has updated record id $id in setting.");
					return $this->response->redirect('settings');
				}
				else {
					$this->flash->error('Pri upravovaní nastala chyba.');
					$this->logger->info('User ' . $this->auth->getName() . " has failed to update record $id in setting.");
					return $this->response->redirect('settings');
				}
			}
			else {
				return true;
			}
		}
		else {
			return $this->response->redirect('errors/show404');
		}
	}

}

