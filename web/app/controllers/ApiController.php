<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 3.9.2014
 * Time: 21:46
 */

class ApiController extends ControllerBase {

	public function getSuploAction($date) {
		$this->view->disable();
		$result = array();
		if ($this->request->isGet()) {
			$suplo = Suplo::find(array(
				'order' => "sup_nick ASC",
				"conditions"	=> "sup_date = CURDATE()"
			));
			$result['suploRecords'] = array();
			$result['suploDate'] = $date->format('Y-m-d');
			if ($suplo) {
				foreach ($suplo as $record) {
					$row = array();
					$row['hour'] = $record->getHour()->tmt_label;
					$row['missing'] = $record->getMissing()->getFullName();
					$row['classes'] = $record->sup_classes;
					$row['subject'] = $record->sup_subject;
					$row['classroom'] = $record->sup_classroom;
					$row['owner'] = $record->getUser()->getFullName();
					$row['note'] = $record->sup_note;
					$result['suploRecords'][] = $row;
				}
			}
			echo json_encode($result);
			return true;
		}
		else {
			return $this->response->setStatusCode(403, 'Access forbiden.');
		}
	}

	public function getActualSuploAction() {
		$this->view->disable();
		$result = array();
		if ($this->request->isGet()) {
			$sql = "SELECT DISTINCT sup_date FROM suplo WHERE sup_date >= CURDATE()";
			$suploDays = $this->db->fetchAll($sql, Phalcon\Db::FETCH_OBJ);
			if ($suploDays) {
				$result['empty'] = false;
				$result['days'] = array();
				foreach ($suploDays as $day) {
					$result['days'][] = $day->sup_date;
				}
			}
			else {
				$result['empty'] = true;
				$result['days'] = null;
			}
			echo json_encode($result);
			return true;
		}
		else {
			return $this->response->setStatusCode(403, 'Access forbiden.');
		}
	}

	public function getPublicEventsAction() {
        $this->view->disable();
        $result = array();
        if ($this->request->isGet()) {
            $publicEvents = Event::find(
                [
                    "order" => "evt_date ASC",
                    "limit" => 10,
                    "columns" => "evt_date, evt_shortDescription, evt_uid",
                    "evt_public = true AND evt_date >= CURDATE()"
                ]
            );
            if ($publicEvents->count() != 0) {
                $result['empty'] = false;
                $result['events'] = array();
                foreach ($publicEvents as $publicEvent) {
                    $result['events'][] = $publicEvent;
                }
            }
            else {
                $result['empty'] = true;
                $result['events'] = null;
            }
            echo json_encode($result);
            return true;
        }
        else {
            return $this->response->setStatusCode(403, 'Access forbiden.');
        }
    }

}