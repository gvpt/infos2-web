<?php

class IndexController extends ControllerBase {

    public function indexAction() {
		if ($this->auth->isLoggedIn()) {
			return $this->dispatcher->forward(array(
				'controller' => 'dashboard',
				'action' => 'index'
			));
		}
		else {
			$systemMessage = SystemMessage::findFirst(array(
				"conditions"	=> "sysm_type = 0 AND sysm_deadline >= CURDATE()"
			));
			if ($systemMessage) {
				$this->view->setVar('systemMessage', $systemMessage);
			} else {
				$this->view->setVar('systemMessage', false);
			}
			
			return true;
		}
	}

	public function viewAction($uid) {
		$announcement = Announcement::findFirstByann_uid($uid);
		if ($announcement->ann_public) {
			$this->view->setVar('announcement', $announcement);
			return true;
		}
		else {
			return $this->response->redirect('errors/show404');
		}
	}

	public function viewEventAction($uid) {
	    $event = Event::findFirstByevt_uid($uid);
        if ($event->evt_public) {
            $this->view->setVar('event', $event);
            return true;
        } else {
            return $this->response->redirect('errors/show404');
        }
    }
}

