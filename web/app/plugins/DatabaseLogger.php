<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 10.8.2014
 * Time: 19:05
 */

use Phalcon\Logger\Adapter;
use Phalcon\Logger\AdapterInterface;
use Phalcon\Logger\Exception;

class DatabaseLogger extends Adapter implements AdapterInterface {

	protected $options;

	/**
	 * @var \Phalcon\Db\Adapter\Pdo\Mysql
	 */
	protected $connection;
	/**
	 * @var string
	 */
	protected $tableName;
	/**
	 * @var string Name of Logger
	 */
	protected $name = "WEB";

	/**
	 * @param array $options
	 * @throws \Phalcon\Logger\Exception
	 */
	function __construct($options = array()) {
		if (!isset($options['db'])) {
			throw new Exception("Parameter 'db' is required");
		}

		if (!isset($options['table'])) {
			throw new Exception("Parameter 'table' is required");
		}

		$this->connection = $options['db'];
		$this->tableName = $options['table'];
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return \Phalcon\Logger\Formatter\Line
	 */
	public function getFormatter() {

	}

	/**
	 * @param string $message
	 * @param integer $type
	 * @param integer $time
	 * @param array $context
	 * @return bool
	 */
	public function logInternal($message, $type, $time, $context=array()) {
		return $this->connection->execute('INSERT INTO ' . $this->tableName . ' VALUES (null, ?, ?, ?, ?)', array($time, $this->name, $type, $message));
	}

	/**
	 * {@inheritdoc}
	 * @return bool
	 */
	public function close() {
		return $this->connection->close();
	}
} 