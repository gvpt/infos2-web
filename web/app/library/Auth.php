<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 16.7.2014
 * Time: 16:43
 */

use Phalcon\Mvc\User\Component;
class Auth extends Component {

	public function authenticate($credentials) {
		$user = User::findFirst(array(
			'conditions' => 'id_user = ?1',
			'bind' => array(
				1 => $credentials['id']
			)
		));
		if ($user != false) {

            $setting = Setting::findFirstById_user($credentials['id']);
            if ($setting == false) {
                $setting = new Setting();
                $setting->set_logoutGoogle = 0;
                $setting->id_user = $credentials['id'];
                $setting->create();
                $setting = Setting::findFirstById_user($credentials['id']);
            }

			$credentials['name'] = $user->usr_firstName . ' ' . $user->usr_lastName;
			$credentials['role'] = $user->role->rol_name;
			$credentials['logoutGoogle'] = $setting->set_logoutGoogle;
			$this->session->set('auth-identity', $credentials);
			$this->logger->info("User " . $credentials['name'] . " has been successfully logged in.");
			return true;
		}
		else {
			$this->logger->warning("User with e-mail " . $credentials['email'] . " has tried to log in.");
			return false;
		}
	}

    public function createLoginUrl() {
        require_once (__DIR__.'/../../vendor/autoload.php');
        $client = new Google_Client();
        $client->setClientId($this->config->google->clientId);
        $client->setRedirectUri($this->config->application->baseUri . "auth/login");
		$client->setPrompt('select_account');
		$client->setApprovalPrompt('');
        $client->setAccessType("offline");
        $client->addScope("email");
        $client->addScope("profile");
        $auth_url = $client->createAuthUrl();
        return $auth_url;
    }


	public function remove() {
		$this->session->remove('auth-identity');
	}

	public function getIdentity() {
		return $this->session->get('auth-identity');
	}

	public function getName() {
		$identity = $this->session->get('auth-identity');
		return $identity['name'];
	}

	public function getEmail() {
		$identity = $this->session->get('auth-identity');
		return $identity['email'];
	}

	public function hasRefreshToken() {
		$identity = $this->session->get('auth-identity');
		return !empty($identity['token']['refresh']);
	}

	public function isLoggedIn() {
		if($this->session->get('auth-identity')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function isAdmin() {
		return ($this->getIdentity()['role'] == 'Administrator' || $this->getIdentity()['role'] == 'Root');
	}

	public function isRoot() {
		return ($this->getIdentity()['role'] == 'Root');
	}

	public function logoutGoogle(){
		$identity = $this->session->get('auth-identity');
		if($identity['logoutGoogle'] == 1) {
			return true;
		};
		return false;
	}
} 