<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 14.7.2014
 * Time: 20:00
 */

use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Resource as AclResource;
use Phalcon\Acl\Role as AclRole;
use Phalcon\Mvc\User\Component;

class Acl extends Component{

	private $acl;

	private $filePath = '/cache/acl/data.cache';

	private $privateResources = array(
		'dashboard' => [
			'index',
			'welcome'
		],
		'announcements' => [
			'index',
			'view',
			'new',
			'edit',
			'delete',
            'list',
            'moveUp'
		],
		'events' => [
			'index',
			'view',
			'new',
			'edit',
			'delete',
			'list'
		],
		'profile' => [
			'index'
		],
		'suplo' => [
			'index',
			'view',
			'new',
			'record',
			'summary',
			'exists'
		],
		'settings' => [
			'index',
			'editSetting',
			'deleteNewsletter',
			'editNewsletter',
			'newNewsletter',
		],
		'system' => [
			'index',
			'editSystemMessage',
			'editSystemNotification'
		]
	);

	public function getResources() {
		return $this->privateResources;
	}

	public function isPrivate($controller) {
		return isset($this->privateResources[$controller]);
	}

	public function isAllowed($role, $controller, $action) {
		return $this->getAcl()->isAllowed($role, $controller, $action);
	}

	/**
	 * @return AclMemory
	 */
	public function getAcl() {
		/*if (is_object($this->acl)) {
			return $this->acl;
		}

		if (function_exists('apc_fetch')) {
			$acl = apc_fetch('infos2-acl');
			if (is_object($acl)) {
				$this->acl = $acl;
				return $acl;
			}
		}*/

		if (!file_exists(APP_DIR . $this->filePath)) {
			$this->acl = $this->rebuild();
			return $this->acl;
		}

		$data = file_get_contents(APP_DIR . $this->filePath);
		$this->acl = unserialize($data);

		if (function_exists('apc_store')) {
			apc_store('infos2-acl', $this->acl);
		}

		return $this->acl;
	}

	public function rebuild() {
		$acl = new AclMemory();

		$acl->setDefaultAction(\Phalcon\Acl::DENY);

		$roles = Role::find('rol_active = 1');

		foreach ($roles as $role) {
			$acl->addRole(new AclRole($role->rol_name));
		}


		foreach ($this->privateResources as $controller => $action) {
			$acl->addResource(new AclResource($controller), $action);
		}

		foreach ($roles as $role) {
			/**
			 * @var Permission permission
			 */
			foreach($role->getPermission() as $permission) {
				$acl->allow($role->rol_name, $permission->per_resource, $permission->per_action);
			}
		}

		if (touch(APP_DIR . $this->filePath) && is_writable(APP_DIR . $this->filePath)) {
			file_put_contents(APP_DIR . $this->filePath, serialize($acl));

			if(function_exists('apc_store')) {
				apc_store('infos2-acl', $acl);
			}
		}
		else {
			$this->logger->error('The user does not have write permissions to create the ACL list at ' . APP_DIR . $this->filePath);
		}

		return $acl;
	}

}